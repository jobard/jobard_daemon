"""Cluster type."""
from enum import Enum


class Cluster(str, Enum):
    """Cluster types."""

    LOCAL = 'LOCAL'
    SWARM = 'SWARM'
    PBS = 'PBS'
    HTCONDOR = 'HTCONDOR'
    KUBE = 'KUBE'
