
def parse_human_size(s):
    """Parse byte string to numbers"""

    s = s.replace(' ', '')
    if not any(char.isdigit() for char in s):
        s = '1' + s

    for i in range(len(s) - 1, -1, -1):
        if not s[i].isalpha():
            break
    index = i + 1

    prefix = s[:index]
    suffix = s[index:]

    return int(float(prefix) * byte_sizes[suffix.lower()])


byte_sizes = {
    'kB': 10**3,
    'MB': 10**6,
    'GB': 10**9,
    'TB': 10**12,
    'PB': 10**15,
    'KiB': 2**10,
    'MiB': 2**20,
    'GiB': 2**30,
    'TiB': 2**40,
    'PiB': 2**50,
    'B': 1,
    '': 1,
}
byte_sizes = {k.lower(): v for k, v in byte_sizes.items()}
byte_sizes.update({k[0]: v for k, v in byte_sizes.items() if k and 'i' not in k})
byte_sizes.update({k[:-1]: v for k, v in byte_sizes.items() if k and 'i' in k})
