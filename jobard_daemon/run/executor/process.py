"""Jobard Job Array executor."""
import pathlib
import sys
from datetime import datetime
from pathlib import Path
from typing import List

import asyncpg
from asyncpg import Record

from jobard_daemon.core.exceptions import JobardError
from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.model.comm.ssh import RemoteSSHConfig
from jobard_daemon.model.comm.tunnel import ReverseTunnelConfig
from jobard_daemon.model.docker.image import DockerImageProperties
from jobard_daemon.model.event.app import AppEvent
from jobard_daemon.model.event.cancel import JobCancelEvent
from jobard_daemon.model.event.job import JobEvent
from jobard_daemon.model.event.progress import JobProgressEvent
from jobard_daemon.model.users.users import RemoteUserProperties
from jobard_daemon.run.processor.dask import DaskOverSSHApplicationProcessor
from jobard_daemon.tcp.server import TCPServer
from jobard_daemon.type.app import AppType
from jobard_daemon.type.cluster import Cluster
from jobard_daemon.type.state import State
from jobard_daemon.utils.configuration import (
    JobardDaemonSettings,
    get_int_conf_from_dict,
    get_str_conf_from_dict,
    parse_job_extra_conf,
)
from jobard_daemon.utils.job_logs import is_default_job_order_name, sanitize_command_alias, sanitize_job_prefix, \
    build_job_log_files, build_jobs_log_path, build_job_array_daemon_log_path

# global app-level postgresql lock for this type of executor
lock_process_job_array_id = 2040403042

# TODO : use a connection pool, PER ASYNCIO LOOP / SPECIFIC PROCESS
# to avoid asyncpg "cannot perform operation: another operation is in progress", we need to assign different connection
# per coroutines, to avoid any resources contention
JOBARD_DAEMON_ASYNCPG_CONNECTION_0 = 0
JOBARD_DAEMON_ASYNCPG_CONNECTION_1 = 1
JOBARD_DAEMON_ASYNCPG_CONNECTION_2 = 2
JOBARD_DAEMON_ASYNCPG_CONNECTION_3 = 3


class JobOrderJobArrayExecutor:
    """Jobard parser executor."""

    # AES key used for ssh passwords and keys decryption
    aes_key = None

    @staticmethod
    async def async_asyncpg_connect(
        loop,
        config,
        executor_id: int,
        conn_id: int,
    ):
        return await asyncpg.connect(
            dsn=config.database.dsn.format(),
            loop=loop,
            timeout=config.database.connect_timeout,
            server_settings={
                'application_name': 'jobard - process_job_array - daemon=' + str(config.core.daemon_id) +
                ' executor=' + str(executor_id) + ' conn=' + str(conn_id)
            }
        )

    @staticmethod
    async def read_user_properties(
            _logger,
            config,
            conn,
            connection_name: str,
            user_id: int,
    ) -> RemoteUserProperties:
        """
        Read in database the remote user properties.
        Args:
            _logger: The logger.
            config: The daemon configuration.
            conn: The database connection.
            connection_name: The remote connection name.
            user_id: The user id.
        """
        _logger.info('read the user properties')

        row = await conn.fetchrow(
            """
            SELECT 
              "cla"."log_root" AS "log_root_path",
              "cla"."remote_app_path" AS "remote_app_path",
              "cla"."entry_point_wrapper" AS "entry_point_wrapper",
              "cla"."entry_point" AS "entry_point",
              "j_user"."unix_uid" AS "unix_uid", 
              -- credentials are taken from cluster if remote_username is present and from user otherwise
              COALESCE(
                "cl"."remote_username", "j_user"."remote_username"
              ) AS "remote_username", 
              -- decrypt password
              CASE WHEN "cl"."remote_username" is not null THEN PGP_SYM_DECRYPT(
                "cl"."password" :: bytea, $4
              ) ELSE PGP_SYM_DECRYPT(
                "j_user"."password" :: bytea, $4
              ) END AS "remote_password",
               -- decrypt ssh key
              CASE WHEN "cl"."remote_username" is not null THEN PGP_SYM_DECRYPT(
                "cl"."client_key" :: bytea, $4
              ) ELSE PGP_SYM_DECRYPT(
                "j_user"."client_key" :: bytea, $4
              ) END AS "remote_client_key"
            FROM 
              "remote_connection" AS "rc" 
              INNER JOIN "cluster_access" AS "cla" ON "cla"."cluster_id" = "rc"."cluster_id" 
              INNER JOIN "cluster" AS "cl" ON "cl"."cluster_id" = "rc"."cluster_id" 
              INNER JOIN "jobard_user" as "j_user" ON "j_user"."user_id" = "cla"."user_id" 
            WHERE 
              "j_user"."user_id" = $2
              AND "rc"."name" = $1
              AND "rc"."daemon_id" = $3
            """,
            connection_name, user_id, str(config.core.daemon_id), JobOrderJobArrayExecutor.aes_key,
            timeout=config.executors.processor.read_remote_user_properties_timeout
        )

        return RemoteUserProperties(remote_username=row['remote_username'],
                                    log_root_path=row['log_root_path'],
                                    remote_app_path=row['remote_app_path'],
                                    entry_point_wrapper=row['entry_point_wrapper'],
                                    entry_point=row['entry_point'],
                                    remote_password=row['remote_password'],
                                    remote_client_key=row['remote_client_key'],
                                    unix_uid=row['unix_uid'])

    @staticmethod
    async def read_image_properties(
            _logger,
            config,
            conn,
            image_id: int,
    ) -> DockerImageProperties:
        """
        Read in database the Docker image properties.
        Args:
            _logger: The logger.
            config: The daemon configuration.
            conn: The database connection.
            image_id: The image id.
        """
        _logger.info('read the docker image properties')

        row = await conn.fetchrow(
            """
            SELECT 
              "i"."name" AS "image_name",
              "i"."url" AS "image_url",
              COALESCE(ARRAY_AGG("mp"."name"  || ' ' || "mp"."source"  || ' ' ||  "mp"."destination"), NULL) AS "mount_points"
            FROM 
              "docker_image" AS "i" 
              LEFT JOIN "docker_mount_point" AS "mp" ON "i"."image_id" = "mp"."image_id" 
            WHERE 
              "i"."image_id" = $1
            GROUP BY "image_name", "image_url"
            """,
            image_id,
            timeout=config.executors.processor.read_docker_image_properties_timeout
        )

        return DockerImageProperties(name=row['image_name'],
                                     url=row['image_url'],
                                     mount_points={mp.split()[0]: (mp.split()[1], mp.split()[2])
                                                   for mp in row['mount_points'] if mp}
                                     if row['mount_points'] else {}
                                     )

    @staticmethod
    async def async_process_job_array(
            loop,
            executor_id: int,
            config: JobardDaemonSettings,
            _logger: JobardLogger,
    ):

        _logger.info('start')

        # connect to the database (connection ID = 0)
        conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_0
        )

        job_order_to_process = 0
        job_array_to_process = 0
        try:

            # open the transaction
            async with conn.transaction(isolation='read_committed', readonly=False):

                # lock the transaction
                await conn.execute(
                    'SELECT pg_advisory_xact_lock(' + str(lock_process_job_array_id) + ');',
                    timeout=config.executors.processor.lock_timeout,
                )

                # get a job array to process
                row = await conn.fetchrow(
                    'SELECT "jo"."id" AS "job_order_id", "ja"."id" AS "job_array_id",'
                    ' "jo"."command" AS "jo_command", "jo"."command_alias" AS "jo_command_alias",'
                    ' "jo"."name" AS "jo_name", "jo"."job_log_prefix_arg_idx" AS "job_log_prefix_arg_idx",'
                    ' "ja"."try_nb" AS "job_array_try_nb" '
                    ' FROM "job_order" AS "jo"'
                    ' INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id"'
                    ' WHERE ('
                    ' "jo"."state" = \'NEW\' OR '
                    ' "jo"."state" = \'ENQUEUED\' OR '
                    ' "jo"."state" = \'SUBMITTED\' OR '
                    ' "jo"."state" = \'RUNNING\''
                    ') AND "jo"."concurrency" < "jo"."max_concurrency"'
                    ' AND "ja"."try_nb" <= $1'
                    ' AND "ja"."state" = \'NEW\''
                    ' ORDER BY "jo"."priority" ASC, (case "jo"."state"'
                    '            when \'RUNNING\' then 1'
                    '            when \'ENQUEUED\' then 2'
                    '            when \'SUBMITTED\' then 3'
                    '            when \'NEW\' then 4'
                    '         end) ASC, "jo"."created" ASC'
                    ' LIMIT 1',
                    config.executors.processor.job_array_retries_max_nb+1
                    )

                if row is not None:
                    job_order_to_process = int(row['job_order_id'])
                    job_array_to_process = int(row['job_array_id'])
                    job_array_try_nb = row['job_array_try_nb']

                    # If the job order name is not a default value, get it
                    job_order_name = row['jo_name'] if not is_default_job_order_name(row['jo_name']) else None
                    # Get the command alias specified by the user or take the first element of the command if exists
                    job_order_command_alias = row['jo_command_alias'] \
                        if row['jo_command_alias'] else sanitize_command_alias(row['jo_command'][0])

                    job_order_job_log_prefix_arg_idx = row['job_log_prefix_arg_idx']

                    # update
                    await conn.execute(
                        'UPDATE "job_array" SET "state" = \'ENQUEUED\' WHERE "id" = $1 AND "state" = \'NEW\'',
                        job_array_to_process,
                        timeout=config.executors.processor.update_job_array_state_timeout,
                    )

                    # update
                    await conn.execute(
                        'UPDATE "job_order" SET "concurrency" = "concurrency" + 1 WHERE "id" = $1',
                        job_order_to_process,
                        timeout=config.executors.processor.update_concurrency_job_order_timeout,
                    )

            # process the job array
            if job_array_to_process > 0:

                _logger.info(
                    'job array id = ' + str(job_array_to_process) + ', ' +
                    'now fetching job order info'
                )

                try:

                    # get details from job order entity
                    row = await conn.fetchrow(
                        """
                        SELECT 
                            "connection", "user_id", "image_id", "job_extra", "env_extra", "cores", "memory", 
                            "walltime", "name"
                        FROM "job_order"
                        WHERE "id" = $1
                        """,
                        job_order_to_process,
                        timeout=config.executors.processor.select_single_job_order_timeout
                    )

                    _logger.info(
                        'job array id = ' + str(job_array_to_process) + ', ' +
                        'job order info fetched'
                    )

                    # parse job extra
                    job_extra = parse_job_extra_conf(row['job_extra'])

                    worker_memory = row['memory']
                    if 'B' not in worker_memory:
                        worker_memory = worker_memory + 'B'
                    walltime = row['walltime']

                    _logger.info(
                        'job array id = ' + str(job_array_to_process) + ', ' +
                        'now fetching jobs'
                    )
                    sys.stdout.flush()

                    # create an empty job event list
                    jobs: list[JobEvent] = []

                    # fetch the list of jobs from the database
                    jobs_tmp: list[Record] = await conn.fetch(
                        'SELECT "j"."id" AS "id", "j"."command_with_parameters" AS "command", "j"."arguments" AS "args"'
                        ' FROM "job" AS "j"'
                        ' INNER JOIN "job_array" AS "ja" ON "j"."job_array" = "ja"."id"'
                        ' WHERE "ja"."id" = $1'
                        ,
                        job_array_to_process
                        , timeout=config.executors.processor.select_many_job_timeout  # large timeout here
                    )

                    _logger.info(
                        'job array id = ' + str(job_array_to_process) + ', ' +
                        'jobs fetched'
                    )

                    # read the remote user properties for the joborder
                    user_properties = await JobOrderJobArrayExecutor.read_user_properties(
                        _logger=_logger,
                        config=config,
                        conn=conn,
                        connection_name=row['connection'],
                        user_id=row['user_id'],
                    )

                    # set root log path
                    default_root_log_path = user_properties.log_root_path \
                        if user_properties.log_root_path else config.executors.processor.remote_app_log_chroot_default
                    log_chroot = pathlib.Path(get_str_conf_from_dict(
                        job_extra,
                        'log.chroot',
                        default_root_log_path,
                    ))
                    await conn.execute(
                        'UPDATE "job_order" SET "root_log_path" = $1 WHERE "id" = $2 AND "root_log_path" IS NOT NULL',
                        log_chroot.as_posix(),
                        job_order_to_process,
                        timeout=config.executors.processor.update_job_order_state_timeout
                    )

                    # convert asyncpg record to JobEvent
                    for job_tmp in jobs_tmp:
                        job_prefix = sanitize_job_prefix(job_tmp['args'][job_order_job_log_prefix_arg_idx]) \
                            if job_order_job_log_prefix_arg_idx is not None else None
                        jobs.append(
                            JobEvent(id=job_tmp['id'], command=job_tmp['command'],
                                    # Retrieve the job prefix in the job args
                                     job_prefix=job_prefix
                                     )
                        )

                    # build the remote host spec
                    conn_name = row['connection']

                    if conn_name is None:
                        raise JobardError('host connection name is required')

                    if conn_name not in config.hosts_connections:
                        raise JobardError('unable to find the host connection with name ' + conn_name)

                    ssh_host = config.hosts_connections[conn_name]

                    # check for Swarm and Kubernetes clusters if a Docker image is defined
                    if ssh_host.cluster in [Cluster.SWARM, Cluster.KUBE] and row['image_id'] is None:
                        raise JobardError(f'Docker image is mandatory for {ssh_host.cluster} cluster. Please create one.')

                    # read the docker image properties for the job order
                    image_properties = None
                    if row['image_id'] is not None:
                        image_properties = await JobOrderJobArrayExecutor.read_image_properties(
                            _logger=_logger,
                            config=config,
                            conn=conn,
                            image_id=row['image_id'],
                        )

                    # initialize remote SSH config
                    base_remote_port = ssh_host.base_remote_port

                    remote = RemoteSSHConfig(
                        host=ssh_host.host,
                        port=ssh_host.port,
                        entry_point_wrapper=user_properties.entry_point_wrapper,
                        app_path=user_properties.remote_app_path,
                        cwd_path=user_properties.remote_app_path or Path('.'),
                        entry_point=user_properties.entry_point,
                        username=user_properties.remote_username,
                        password=user_properties.remote_password,
                        client_keys=[user_properties.remote_client_key] if user_properties.remote_client_key else None,
                        connect_timeout=ssh_host.connect_timeout,
                    )
                    # app spec
                    app = AppEvent(
                        daemon_id=config.core.daemon_id,
                        executor_id=executor_id,

                        job_order_id=job_order_to_process,
                        job_array_id=job_array_to_process,
                        try_id=job_array_try_nb,
                        job_order_name= job_order_name,
                        command_alias= job_order_command_alias,
                        name=row['name']+'_'+str(job_array_to_process),
                        # TODO : set from a future "app_type" property of the job order
                        app_type=AppType.DASK_JOB_ARRAY,
                        cluster_type=ssh_host.cluster,

                        # TODO : override it by property set in the job order
                        driver_cores=1,
                        driver_memory='2GB',

                        # TODO : WARN handle errors correctly. ONLY 1 CORE for "sequentiel" queue !
                        # TODO : see Issue #5 from "Jobard Remote Dask"
                        worker_cores=1,  # worker_cores / row['cores'] ?,
                        worker_memory=worker_memory,

                        n_workers=get_int_conf_from_dict(
                            job_extra,
                            'n.workers',
                            config.executors.processor.remote_app_workers_per_job_array_default,
                        ),
                        # TODO : support dynamic workers
                        n_min_workers=None,
                        n_max_workers=None,

                        log_chroot=log_chroot,

                        queue=get_str_conf_from_dict(
                            job_extra,
                            'queue',
                            config.executors.processor.remote_app_queue_default
                        ),

                        walltime=walltime,

                        application_processor_timeout=get_int_conf_from_dict(
                            job_extra,
                            'application.processor.timeout',
                            config.executors.processor.remote_app_application_processor_timeout_default,
                        ),

                        system_limit_timeout=get_int_conf_from_dict(
                            job_extra,
                            'system.limit.timeout',
                            config.executors.processor.remote_app_system_limit_timeout_default,
                        ),

                        image=image_properties.url if image_properties is not None else None,
                        docker_mount_points=image_properties.mount_points if image_properties is not None else None,
                        death_timeout=get_int_conf_from_dict(
                            job_extra,
                            'death.timeout',
                            config.executors.processor.remote_app_death_timeout_default,
                        ),

                        unix_uid=user_properties.unix_uid,

                        # TODO : those variables MUST be in the "job order" entity
                        driver_extra_args=None,  # row['job_extra'] ?
                        worker_extra_args=None,
                        jobard_extra_args=None,
                    )

                    # tunnel spec
                    tunnel = ReverseTunnelConfig(
                        local_port=config.executors.processor.base_local_port+executor_id,
                        remote_port=base_remote_port+executor_id
                    )

                    # intercommunication tcp server spec
                    def progress_callback(progress_events: List[JobProgressEvent]) -> None:
                        pass

                    async def async_progress_callback(progress_events: List[JobProgressEvent]) -> None:
                        """Async progress callback event from the remote distributed app."""

                        # connect to the database (connection ID = 1)
                        _conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_1
                        )

                        try:

                            events_len = len(progress_events)
                            if events_len > 0:
                                _logger.info(
                                    'job array id = ' + str(job_array_to_process) + ', ' +
                                    str(events_len) + ' progress events received'
                                )

                            states_values = []
                            jobs_path = build_jobs_log_path(
                                log_chroot=log_chroot,
                                command_alias=job_order_command_alias,
                                job_order_name=job_order_name,
                                job_order_id=job_order_to_process,
                                job_array_id=job_array_to_process,
                                try_id=job_array_try_nb
                            )

                            for progress_event in progress_events:
                                update_finished_date = False
                                if progress_event.state is State.SUCCEEDED or progress_event.state is State.FAILED:
                                    update_finished_date = True
                                update_submitted_date = False
                                if progress_event.state is State.SUBMITTED or progress_event.state is State.ENQUEUED:
                                    update_submitted_date = True

                                (log_file_stdout, log_file_stderr) = build_job_log_files(
                                    job_idx=progress_event.id, job_prefix=progress_event.job_prefix)

                                states_values.append(
                                    (
                                        progress_event.state.upper(),
                                        progress_event.progress,
                                        datetime.utcnow() if update_finished_date is True else None,
                                        datetime.utcnow() if update_submitted_date is True else None,
                                        jobs_path.joinpath(log_file_stdout).as_posix()
                                        if update_finished_date is True else None,
                                        jobs_path.joinpath(log_file_stderr).as_posix()
                                        if update_finished_date is True else None,
                                        progress_event.id,
                                    )
                                )

                            if len(states_values) > 0:

                                # commit the job state
                                statement = """UPDATE "job"
                                            SET "state" = $1, "progress" = $2,
                                            "finished" = COALESCE($3, "finished"),
                                            "submitted" = COALESCE($4, "submitted"),
                                            "log_path_stdout" = $5, "log_path_stderr" = $6 
                                            WHERE "id" = $7;
                                """
                                await _conn.executemany(
                                    statement,
                                    states_values,
                                    timeout=config.executors.processor.update_many_job_at_once_timeout,
                                )

                                # TODO : better RUNNING state handling
                                await _conn.execute(
                                    'UPDATE "job_array" '
                                    ' SET "state" = \'RUNNING\''
                                    ' WHERE "id" = $1 AND "state" = \'ENQUEUED\'',
                                    job_array_to_process,
                                    timeout=config.executors.processor.update_job_array_state_timeout,
                                )

                                # update job order state
                                await JobOrderJobArrayExecutor.update_job_order(
                                    _logger, config, _conn, job_order_to_process
                                )
                        finally:
                            await _conn.close()

                    def cancel_callback_server_ack(cancel_events: List[JobCancelEvent]) -> None:
                        pass

                    async def async_cancel_callback_server_ack(cancel_events: List[JobCancelEvent]) -> None:
                        """Async ack cancel callback event from the remote distributed app."""

                        # connect to the database (connection ID = 2)
                        _conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_2
                        )

                        try:

                            events_len = len(cancel_events)
                            if events_len > 0:
                                _logger.info(
                                    'job array id = ' + str(job_array_to_process) + ', ' +
                                    str(events_len) + ' cancel events received'
                                )

                            try:

                                states_values = []
                                for cancel_event in cancel_events:
                                    # TODO : implement real job progress
                                    states_values.append((State.CANCELLED, 0.0, cancel_event.id))

                                if len(states_values) > 0:

                                    # commit the job state
                                    statement = """UPDATE "job"
                                                SET "state" = $1, "progress" = $2
                                                WHERE "id" = $3;
                                    """
                                    await _conn.executemany(
                                        statement,
                                        states_values,
                                        timeout=config.executors.processor.update_many_job_at_once_timeout,
                                    )

                                # update job order state
                                await JobOrderJobArrayExecutor.update_job_order(
                                    _logger, config, _conn, job_order_to_process
                                )

                            except Exception:
                                # TODO : ignore for now
                                pass
                        finally:
                            await _conn.close()

                    async def async_cancel_check_callback() -> List[JobCancelEvent]:
                        """Async callback to check if we have job to cancel or not."""

                        _logger.info('doing cancel check')

                        # connect to the database (connection ID = 3)
                        _conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_3
                        )

                        try:
                            # TODO : cancel unit job, or cancel the whole job order ?
                            tmp: Record = await _conn.fetchrow(
                                'SELECT count("state") AS "count" '
                                'FROM ( '
                                '    SELECT "j"."state" '
                                '    FROM "job" AS "j" '
                                '    INNER JOIN "job_array" AS "ja" ON "j"."job_array" = "ja"."id" '
                                '    WHERE "ja"."id" = $1 '
                                '    UNION ALL '
                                '    SELECT "ja"."state" '
                                '    FROM "job_array" AS "ja" '
                                '    WHERE "ja"."id" = $1 '
                                '    UNION ALL '
                                '    SELECT "jo"."state" '
                                '    FROM "job_order" AS "jo" '
                                '    INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id" '
                                '    WHERE "ja"."id" = $1 '
                                ') AS "t2" '
                                'WHERE "state" = \'CANCELLATION_ASKED\'',
                                job_array_to_process
                                , timeout=config.executors.processor.select_many_job_timeout  # large timeout here
                            )

                            cancel_events = []

                            if tmp['count'] > 0:
                                for job in jobs:
                                    cancel_events.append(JobCancelEvent(id=job.id))

                        finally:
                            await _conn.close()

                        return cancel_events

                    # create the tcp server
                    server = TCPServer(
                        timeout=config.executors.processor.tcp_server_timeout,
                        connect_timeout=config.executors.processor.tcp_server_connect_timeout,
                        host=tunnel.local_host,
                        port=tunnel.local_port,
                        app=app,
                        jobs=jobs,
                        progress_callback=progress_callback,
                        cancel_callback=cancel_callback_server_ack,
                        async_progress_callback=async_progress_callback,
                        async_cancel_callback=async_cancel_callback_server_ack,
                        loop=loop,
                    )

                    # create the Dask processor spec
                    daemon_log_output_dir = build_job_array_daemon_log_path(
                                log_chroot=log_chroot,
                                command_alias=job_order_command_alias,
                                job_order_name=job_order_name,
                                job_order_id=job_order_to_process,
                                job_array_id=job_array_to_process,
                                try_id=job_array_try_nb
                            )

                    processor = DaskOverSSHApplicationProcessor(
                        loop,
                        daemon_id=config.core.daemon_id,
                        executor_id=executor_id,
                        remote_host_conf=remote,
                        comm_tcp_server=server,
                        tunnel_conf=tunnel,
                        daemon_log_output=daemon_log_output_dir,
                        cancel_check_callback=async_cancel_check_callback,
                        logger=_logger
                    )

                    _logger.info(
                        'job array id = ' + str(job_array_to_process) + ', ' +
                        'running processor'
                    )

                    # update job array state
                    await conn.execute(
                        'UPDATE "job_array" SET "daemon_id" = $1, "executor_id" = $2, "executor_log_path" = $3,'
                        ' "submitted" = NOW() WHERE "id" = $4',
                        str(config.core.daemon_id),
                        str(executor_id),
                        processor.log_file().as_posix(),
                        job_array_to_process,
                        timeout=config.executors.processor.update_job_array_state_timeout
                    )

                    # closing asyncpg main connection
                    await conn.close()
                    conn = None

                    # do long running operation
                    try:
                        await processor.async_process()
                    finally:
                        _logger.info(
                            'job array id = ' + str(job_array_to_process) + ', ' +
                            'processor cleanup'
                        )
                        try:
                            await processor.async_cleanup()
                        except Exception as error:
                            # non-fatal exception
                            _logger.exception(error)
                        if processor.timed_out is True:
                            _logger.warn(
                                'job array id = ' + str(job_array_to_process) + ', ' +
                                'the whole application timed out'
                            )
                            raise JobardError('the job array app ' + str(job_array_to_process) + ' has timed out')

                    # re-open main connection to the database (connection ID = 0), after long running op
                    if conn is None:
                        conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_0
                        )

                    rows: list[Record] = await conn.fetch(
                        'SELECT COUNT("j"."id") AS "total"'
                        ' FROM "job" AS "j"'
                        ' INNER JOIN "job_array" AS "ja" ON "j"."job_array" = "ja"."id"'
                        ' WHERE "ja"."id" = $1 AND "j"."state" = \'SUCCEEDED\' AND "j"."progress" = 1.0'
                        ' UNION ALL'
                        ' SELECT COUNT("j"."id") AS "total"'
                        ' FROM "job" AS "j"'
                        ' INNER JOIN "job_array" AS "ja" ON "j"."job_array" = "ja"."id"'
                        ' WHERE "ja"."id" = $1',
                        job_array_to_process,
                        timeout=config.database.default_timeout
                    )

                    finished_jobs = rows[0]['total']
                    total_jobs = rows[1]['total']

                    _logger.info('detected ' + str(finished_jobs) + ' out of ' + str(total_jobs))

                    if finished_jobs == total_jobs:
                        state_to_commit = 'SUCCEEDED'
                        progress_to_commit = 1.0
                        _logger.info('job array fully succeeded')
                    else:
                        state_to_commit = 'FAILED'
                        progress_to_commit = 0.0
                        _logger.info('wrong number of jobs finished for that job array, marking as failed')

                    # update job array entity
                    await conn.execute(
                        'UPDATE "job_array" SET "state" = $1, "progress" = $2, "finished" = NOW() WHERE "id" = $3'
                        ' AND ("state" = \'ENQUEUED\' OR "state" = \'RUNNING\' OR "state" = \'SUBMITTED\')',
                        state_to_commit,
                        progress_to_commit,
                        job_array_to_process,
                        timeout=config.executors.processor.update_job_array_state_timeout
                    )

                    # update job entities states accordingly
                    await JobOrderJobArrayExecutor.update_job_state_after_job_array_has_finished(
                        _logger,
                        config,
                        conn,
                        job_array_to_process
                    )

                except Exception as error:

                    _logger.warn(
                        'job array id = ' + str(job_array_to_process) + ', ' +
                        'job array try number = ' + str(job_array_try_nb) + ', ' +
                        'an error occured'
                    )

                    _logger.exception(error)

                    # re-open main connection to the database (connection ID = 0), after long running op
                    if conn is None:
                        conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_0
                        )

                    # au moins une erreur est apparue pendant le processing
                    # update the job array state accordingly to job array retry settings
                    await JobOrderJobArrayExecutor.update_job_array_state_after_job_array_has_failed(
                        _logger,
                        config,
                        conn,
                        job_array_to_process,
                        job_array_try_nb,
                        error
                    )

                    # update job entities states accordingly
                    await JobOrderJobArrayExecutor.update_job_state_after_job_array_has_finished(
                        _logger,
                        config,
                        conn,
                        job_array_to_process
                    )

                finally:

                    # re-open main connection to the database (connection ID = 0), after long running op
                    if conn is None:
                        conn = await JobOrderJobArrayExecutor.async_asyncpg_connect(
                            loop, config, executor_id, JOBARD_DAEMON_ASYNCPG_CONNECTION_0
                        )

                    # open the transaction
                    async with conn.transaction(isolation='read_committed', readonly=False):

                        # update job order state
                        await JobOrderJobArrayExecutor.update_job_order(_logger, config, conn, job_order_to_process)

                        # update job order concurrency
                        await conn.execute(
                            'UPDATE "job_order" SET "concurrency" = "concurrency" - 1 WHERE "id" = $1',
                            job_order_to_process,
                            timeout=config.executors.processor.update_concurrency_job_order_timeout,
                        )

            else:
                _logger.info(
                    'nothing to do'
                )

        finally:
            # Close the connection.
            if conn is not None:
                await conn.close()

        _logger.info('end')

        # tell the caller that we have other work to do, so we can trigger another instance of this function immediately
        return job_array_to_process > 0

    @staticmethod
    async def update_job_array_state_after_job_array_has_failed(
            _logger,
            config,
            conn,
            job_array_to_process,
            job_array_try_nb,
            error
    ):

        # if a job array has to be retried
        if job_array_try_nb <= config.executors.processor.job_array_retries_max_nb:
            await conn.execute(
                """UPDATE "job_array"
                    SET "state" = 'NEW', "submitted" = NULL, "try_nb" = $2
                    WHERE "id" = $1 AND 
                    ( 
                    "state" = 'NEW' OR 
                    "state" = 'ENQUEUED' OR 
                    "state" = 'SUBMITTED' OR 
                    "state" = 'RUNNING'
                    )
                """,
                job_array_to_process,
                job_array_try_nb+1,
                timeout=config.executors.processor.update_job_array_state_timeout
            )
        else:
            await conn.execute(
                """UPDATE "job_array"
                    SET "state" = 'FAILED', "finished" = NOW(),
                        "error_message" = CONCAT_WS('|',error_message, $2::text)
                    WHERE "id" = $1 AND 
                    ( 
                    "state" = 'NEW' OR 
                    "state" = 'ENQUEUED' OR 
                    "state" = 'SUBMITTED' OR 
                    "state" = 'RUNNING'
                    )
                """,
                job_array_to_process,
                str(error),
                timeout=config.executors.processor.update_job_array_state_timeout
            )

    @staticmethod
    async def update_job_state_after_job_array_has_finished(
            _logger,
            config,
            conn,
            job_array_to_process
    ):

        # update job entities
        await conn.execute(
            'WITH "t" AS ('
            '	SELECT "j"."id" AS "jid", "ja"."state" AS "state"'
            '	FROM "job_array" AS "ja"'
            '	INNER JOIN "job" AS "j" ON "j"."job_array" = "ja"."id"'
            '	WHERE "ja"."id" = $1 AND '
            '		("j"."state" = \'NEW\' OR "j"."state" = \'ENQUEUED\' OR '
            '        "j"."state" = \'RUNNING\' OR "j"."state" = \'SUBMITTED\')'
            ') '
            'UPDATE "job" '
            'SET "state" = "t"."state" '
            'FROM "t" '
            'WHERE "id" = "t"."jid" ',
            job_array_to_process,
            timeout=config.executors.processor.update_job_state_timeout
        )

    @staticmethod
    async def update_job_order(
            _logger,
            config,
            conn,
            job_order_to_process,
    ):

        _logger.info('updating job_order state')

        # le job_order parent, doit être mis à jour en fonction de l'état de ses job_array
        rows = await conn.fetch(
            'SELECT "t2"."res" AS "res", sum("t2"."count") AS "count"'
            ' FROM ('
            '    SELECT "t"."res" AS "res", count("t"."id") AS "count"'
            '    FROM'
            '    ('
            '        ('
            '           SELECT "ja"."state"::text AS "res", "ja"."id" AS "id"'
            '            FROM "job_order" AS "jo"'
            '            INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id"'
            '            WHERE "jo"."id" = $1'
            '        )'
            '        UNION ALL'
            '        ('
            '            SELECT \'ALL\' AS "res", "ja"."id" AS "id"'
            '            FROM "job_order" AS "jo"'
            '            INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id"'
            '            WHERE "jo"."id" = $1'
            '        )'
            '    ) as "t"'
            '    GROUP BY "t"."res"'
            '    UNION'
            '    SELECT unnest(enum_range(NULL::state))::text as "state", 0 as "count"'
            ') as "t2"'
            'GROUP BY "t2"."res"',
            job_order_to_process,
            timeout=config.executors.processor.determine_job_order_state_timeout
        )

        final_state = None

        states = {}
        for row in rows:
            states[row['res']] = row['count']

        update_finished_date = False
        update_submitted_date = False

        # if all job array SUCCEEDED
        if states['ALL'] == states['SUCCEEDED']:
            final_state = State.SUCCEEDED
            update_finished_date = True
        # if all job array FAILED
        elif states['ALL'] == states['FAILED']:
            final_state = State.FAILED
            update_finished_date = True
        # if all job array FINISHED (SUCCEEDED or FAILED)
        elif states['ALL'] == states['SUCCEEDED'] + states['FAILED']:
            final_state = State.FAILED
            update_finished_date = True
        # if some job array have been marked with CANCELLED state
        elif states['CANCELLED'] > 0 or states['CANCELLATION_ASKED'] > 0:
            final_state = State.CANCELLED
        # if some job array have already finished, we consider the whole job order as RUNNING
        # even if no job array are being processed
        elif states['SUCCEEDED'] > 0 or states['FAILED'] > 0:
            final_state = State.RUNNING
            update_submitted_date = True
        # at least on job array is in running state
        elif states['RUNNING'] > 0:
            final_state = State.RUNNING
            update_submitted_date = True
        # at least one job array is in SUBMITTED state
        elif states['SUBMITTED'] > 0:
            final_state = State.SUBMITTED
            update_submitted_date = True
        # at least one job array is in ENQUEUED state
        elif states['ENQUEUED'] > 0:
            final_state = State.ENQUEUED
            update_submitted_date = True
        # at least one job array is in NEW state
        elif states['NEW'] > 0:
            final_state = State.NEW

        # update
        await conn.execute(
            'UPDATE "job_order"'
            ' SET "state" = $1'
            ' WHERE "id" = $2 AND '
            '  "state" != \'CANCELLATION_ASKED\' AND '
            '  "state" != \'CANCELLED\' ',
            final_state.upper(),
            job_order_to_process,
            timeout=config.executors.processor.update_job_order_state_timeout
        )

        now = datetime.utcnow()

        # update finished job order date
        if update_finished_date is True:
            await conn.execute(
                'UPDATE "job_order"'
                ' SET "finished" = $1'
                ' WHERE "id" = $2 AND "finished" IS NULL',
                now,
                job_order_to_process,
                timeout=config.executors.processor.update_job_order_state_timeout
            )

        # update submitted job order date
        if update_submitted_date is True:
            await conn.execute(
                'UPDATE "job_order"'
                ' SET "submitted" = $1'
                ' WHERE "id" = $2 AND "submitted" IS NULL',
                now,
                job_order_to_process,
                timeout=config.executors.processor.update_job_order_state_timeout
            )

        # update job array progress
        await conn.execute(
            'WITH "t2" AS ('
            '    SELECT "ja"."id" as "jaid", "ja"."job_count" as "total", count(distinct "j"."id") AS "finished"'
            '    FROM "job_order" AS "jo"'
            '    INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id"'
            '    LEFT OUTER JOIN "job" AS "j" ON "j"."job_array" = "ja"."id"'
            ' AND ("j"."state" = \'SUCCEEDED\' OR "j"."state" = \'FAILED\')'
            '    WHERE "jo"."id" = $1'
            '    GROUP BY "ja"."id"'
            ' )'
            ' UPDATE "job_array"'
            ' SET "progress" = "t2"."finished" / CAST("t2"."total" AS FLOAT)'
            ' FROM "t2"'
            ' WHERE "id" = "t2"."jaid"',
            job_order_to_process,
            timeout=config.executors.processor.compute_progress_timeout
        )

        # update job order progress
        await conn.execute(
            'WITH "t4" AS ('
            '    SELECT CAST($1 AS INTEGER) AS "joid", COUNT(DISTINCT "j"."id") AS "finished"'
            '    FROM "job_order" AS "jo"'
            '    INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id"'
            '    INNER JOIN "job" AS "j" ON "j"."job_array" = "ja"."id" '
            '    WHERE "jo"."id" = $1 AND '
            '        ("j"."state" = \'SUCCEEDED\' OR "j"."state" = \'FAILED\')'
            ' )'
            ' UPDATE "job_order"'
            ' SET "progress" = "t4"."finished" / CAST("job_count" AS FLOAT)'
            ' FROM "t4"'
            ' WHERE "id" = "t4"."joid"',
            job_order_to_process,
            timeout=config.executors.processor.compute_progress_timeout
        )
