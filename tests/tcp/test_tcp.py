"""Test TCP Server and Client."""
import asyncio
import pathlib
from typing import List

from jobard_daemon.model.event.app import AppEvent
from jobard_daemon.model.event.cancel import JobCancelEvent
from jobard_daemon.model.event.job import JobEvent
from jobard_daemon.model.event.progress import JobProgressEvent
from jobard_daemon.tcp.client import TCPClient
from jobard_daemon.tcp.server import TCPServer
from jobard_daemon.type.app import AppType
from jobard_daemon.type.cluster import Cluster
from jobard_daemon.type.state import State


def test_tcp():
    timeout = 20
    connect_timeout = 60
    host = '127.0.0.1'
    port = 8822

    app = AppEvent(
        job_order_id=1,
        job_array_id=1,
        try_id=1,
        command_alias='my_command',
        name='plop',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.PBS,

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=1,
        worker_memory='4GB',
        n_workers=10,
        n_min_workers=None,
        n_max_workers=None,

        log_chroot=pathlib.Path(r'/home1/datahome/cerint/jobard/log/'),

        queue='sequentiel',
        walltime='00:30:00',

        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=60,

        driver_extra_args=None,
        worker_extra_args=None,
        jobard_extra_args=None,
    )

    jobs = [
        JobEvent(id=1, command=['binary', 'arg', 'arg1']),
        JobEvent(id=2, command=['binary', 'arg', 'arg2']),
        JobEvent(id=3, command=['binary', 'arg', 'arg3']),
        JobEvent(id=4, command=['binary', 'arg', 'arg4'])
    ]

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    end_event = asyncio.Event(loop=loop)
    server_ready_event = asyncio.Event(loop=loop)

    def progress_callback(job_list: List[JobProgressEvent]) -> None:
        assert job_list[0].id == 4

    def cancel_callback_server_ack(job_list: List[JobCancelEvent]) -> None:
        assert len(job_list) == 0 or job_list[0].id == 2

    server = TCPServer(
        timeout=timeout,
        connect_timeout=connect_timeout,
        host=host,
        port=port,
        app=app,
        jobs=jobs,
        end_event=end_event,
        ready_event=server_ready_event,
        progress_callback=progress_callback,
        cancel_callback=cancel_callback_server_ack,
        loop=loop
    )

    server.add_job_to_cancel(cancel=JobCancelEvent(id=2))

    client = TCPClient(
        timeout=timeout,
        connect_timeout=connect_timeout,
        host=host,
        port=port,
        wait_event=server_ready_event,
        end_event=end_event,
        loop=loop
    )

    async def test_internal_gather(s, c):
        return await asyncio.gather(
            s.run(),
            c()
        )

    async def test_internal_gather_with_arg(s, c, arg):
        return await asyncio.gather(
            s.run(),
            c(arg)
        )

    ret = loop.run_until_complete(test_internal_gather(server, client.ping))
    assert ret[1].message == 'pong'

    end_event.clear()
    server_ready_event.clear()

    ret = loop.run_until_complete(test_internal_gather(server, client.get_app))

    assert ret[1].name == 'plop'

    end_event.clear()
    server_ready_event.clear()

    ret = loop.run_until_complete(test_internal_gather(server, client.get_commands))

    assert ret[1][0].command[0] == 'binary'

    end_event.clear()
    server_ready_event.clear()

    ret = loop.run_until_complete(test_internal_gather(server, client.get_jobs_to_cancel))

    assert ret[1][0].id == 2

    end_event.clear()
    server_ready_event.clear()

    ret = loop.run_until_complete(test_internal_gather(server, client.get_jobs_to_cancel))

    assert len(ret[1]) == 0

    end_event.clear()
    server_ready_event.clear()

    jobs_progress = [
        JobProgressEvent(id=4, progress=1.0, state=State.SUCCEEDED),
        JobProgressEvent(id=1, progress=0.6, state=State.RUNNING),
    ]

    loop.run_until_complete(test_internal_gather_with_arg(server, client.report_progress, jobs_progress))

    assert True

    loop.close()
