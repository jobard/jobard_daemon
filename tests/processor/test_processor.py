import asyncio
import getpass as gt
import os
import pathlib
from typing import List

import pandas as pd
import pytest

from jobard_daemon.model.comm.ssh import RemoteSSHConfig
from jobard_daemon.model.comm.tunnel import ReverseTunnelConfig
from jobard_daemon.model.event.app import AppEvent
from jobard_daemon.model.event.cancel import JobCancelEvent
from jobard_daemon.model.event.job import JobEvent
from jobard_daemon.model.event.progress import JobProgressEvent
from jobard_daemon.run.processor.dask import DaskOverSSHApplicationProcessor
from jobard_daemon.tcp.server import TCPServer
from jobard_daemon.type.app import AppType
from jobard_daemon.type.cluster import Cluster

local_users_only = [
    'candre'  # PC Charlie
]


def test_processor():
    username = gt.getuser()

    pytest.skip('tmp skip')

    if username not in local_users_only:
        pytest.skip('this test is not supported by CI/CD because it requires a GitLab Runner with SSH ability')

    # remote host spec
    host = 'datarmor1-10g.ifremer.fr'
    port = 22
    username = 'cerint'
    entry_point_wrapper = '/home1/datahome/cerint/.conda/envs/jobard_remote_dask/bin/python'
    app_path = '/home1/datahome/cerint/candre/jobard_remote_dask/'
    cwd_path = app_path
    entry_point = 'main.py'

    with open(os.path.dirname(__file__) + '/ssh_password', 'r') as handle:
        password = handle.readline()  # read plain text password, or ssh client auth key

    remote = RemoteSSHConfig(
        host=host,
        port=port,
        entry_point_wrapper=entry_point_wrapper,
        app_path=app_path,
        cwd_path=cwd_path,
        entry_point=entry_point,
        username=username,
        password=password,
        client_keys=None)

    # get loop
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    # app spec
    app = AppEvent(
        job_order_id=1,
        job_array_id=1,
        try_id=1,
        name='plop',
        app_type=AppType.DASK_JOB_ARRAY,
        cluster_type=Cluster.PBS,

        driver_cores=1,
        driver_memory='2GB',

        worker_cores=1,
        worker_memory='4GB',
        n_workers=10,
        n_min_workers=None,
        n_max_workers=None,

        log_chroot=pathlib.Path(r'/home1/datahome/cerint/jobard/log/'),

        queue='sequentiel',
        walltime='00:30:00',

        application_processor_timeout=240,
        system_limit_timeout=120,
        death_timeout=60,

        driver_extra_args=None,
        worker_extra_args=None,
        jobard_extra_args=None,
    )

    # job spec
    df = pd.read_csv(os.path.dirname(__file__) + '/files.csv', header=None)
    df = df.reset_index()
    jobs: list[JobEvent] = []
    for index, row in df.iterrows():
        file = row[0]
        jobs.append(
            JobEvent(
                id=index,
                command=[
                    '/home1/datahome/cerint/.conda/envs/felyx_processor/bin/python',
                    '/home1/datahome/cerint/.conda/envs/felyx_processor/bin/felyx-extraction',
                    '-c',
                    '/home1/datahome/cerint/candre/felyx_data_maxss.yaml',
                    '--dataset_id',
                    'SEALEVEL_GLO_PHY_L3_REP_OBSERVATIONS_008_062_S3A',
                    '--miniprod_dir',
                    '/home1/datawork/cerint/candre/data/',
                    '--manifest_dir',
                    '/home1/datawork/cerint/candre/manifests/',
                    '--inputs',
                    file,
                ]
            )
        )

    # tunnel spec
    tunnel = ReverseTunnelConfig(
        local_port=8822,
        remote_port=10822
    )

    # intercommunication tcp server spec
    def progress_callback(job_list: List[JobProgressEvent]) -> None:
        print(job_list)

    def cancel_callback_server_ack(job_list: List[JobCancelEvent]) -> None:
        pass

    server = TCPServer(
        timeout=20,
        connect_timeout=60,
        host=tunnel.local_host,
        port=tunnel.local_port,
        app=app,
        jobs=jobs,
        progress_callback=progress_callback,
        cancel_callback=cancel_callback_server_ack,
        loop=loop
    )

    # processor spec
    processor = DaskOverSSHApplicationProcessor(
        loop,
        daemon_id=0,
        executor_id=0,
        remote_host_conf=remote,
        comm_tcp_server=server,
        tunnel_conf=tunnel,
        daemon_log_output=pathlib.Path('/tmp/'),
    )

    # run
    try:
        processor.process()
    finally:
        processor.cleanup()

    # close loop
    loop.close()

    assert True
