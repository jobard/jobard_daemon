"""RequestHeaderEvent class."""
from pydantic import NonNegativeInt

from jobard_daemon.model.base import JobardModel
from jobard_daemon.type.op import Op


class RequestHeaderEvent(JobardModel):
    """RequestHeaderEvent class."""

    op: Op                                           # request header op
    obj_count: NonNegativeInt                           # number of objects into the request body
