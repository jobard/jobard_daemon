from __future__ import annotations

import getpass
import tempfile
from pathlib import Path
from typing import Dict, List, Optional

import yaml
from pydantic import BaseModel, BaseSettings, NonNegativeInt, PositiveInt, PostgresDsn, validator
from pydantic.datetime_parse import timedelta
from pydantic.types import PositiveFloat

from jobard_daemon.type.basic import Memory, Walltime
from jobard_daemon.type.cluster import Cluster
from jobard_daemon.utils.merger import deep_merge_dicts


class JobardSettings(BaseSettings):
    """Jobard Settings root class."""
    class Config:  # noqa: D106, WPS431
        validate_all = True
        validate_assignment = True
        env_nested_delimiter = '__'

    @classmethod
    def from_yaml_file(cls, file: Path):
        """"Load a model from an YAML file"""
        with open(file) as f:
            return cls(**yaml.safe_load(f))

    def to_yaml(self) -> str:
        """"Dump model to YAML"""
        return yaml.dump(self.dict())

    def merge(self, partial: Dict) -> JobardSettings:
        """Merge from a partial dictionary"""
        if partial is None:
            return self
        merge_dicts = deep_merge_dicts(self.dict(), partial)
        return self.parse_obj(merge_dicts)


class JobardModel(BaseModel):
    """Jobard Model root class."""

    class Config:
        """Validate constraints on assignment"""
        validate_all = True
        validate_assignment = True


class JobardCoreConfig(JobardModel):
    """JobardCoreConfig class."""

    # daemon uniq id
    daemon_id: NonNegativeInt = 0

    # number of "job order to jobs and job arrays parsers" executors (one is enough per daemon)
    nb_parser_executors: PositiveInt = 1

    # number of job array executors (10 is a good basis)
    nb_processor_executors: PositiveInt = 10

    # number of cleanup executors (one is usually enough per daemon)
    nb_cleanup_executors: PositiveInt = 1

    # number of healthcheck executors (one is usually enough per daemon)
    nb_healthcheck_executors: PositiveInt = 1


class JobardDatabaseConfig(JobardModel):
    """JobardDatabaseConfig class."""

    dsn: Optional[PostgresDsn]
    connect_timeout: PositiveFloat = 60
    default_timeout: PositiveFloat = 120
    connect_max_retries: PositiveInt = 3


class JobardParserExecutorConfig(JobardModel):
    """JobardParserExecutorConfig class."""

    # database lock timeout
    lock_timeout: PositiveFloat = 60
    # update job order timeout
    update_job_order_timeout: PositiveFloat = 20
    # select full job order arguments to parse
    select_job_order_arguments_timeout: PositiveFloat = 600
    # delete job or job order
    delete_job_timeout: PositiveFloat = 60
    # select job array
    select_job_array_timeout: PositiveFloat = 20
    # insert many job array
    insert_many_job_array_timeout: PositiveFloat = 600
    # insert many job
    insert_many_job_timeout: PositiveFloat = 1000


class JobardProcessorExecutorConfig(JobardModel):
    """JobardProcessorExecutorConfig class."""

    # min/base local port for TCP exchange
    base_local_port: PositiveInt = 8202
    # database lock timeout
    lock_timeout: PositiveFloat = 60
    # select many job timeout
    select_many_job_timeout: PositiveFloat = 1000
    # select single job order timeout
    select_single_job_order_timeout: PositiveFloat = 20
    # update job state timeout
    update_job_state_timeout: PositiveFloat = 20
    # update job array state timeout
    update_job_array_state_timeout: PositiveFloat = 20
    # update job order state timeout
    update_job_order_state_timeout: PositiveFloat = 20
    # determine job order state timeout
    determine_job_order_state_timeout: PositiveFloat = 60
    # update many job state at once timeout
    update_many_job_at_once_timeout: PositiveFloat = 240
    # update concurrency job order timeout
    update_concurrency_job_order_timeout: PositiveFloat = 20
    # read remote user properties timeout
    read_remote_user_properties_timeout: PositiveFloat = 60
    # read remote user properties timeout
    read_docker_image_properties_timeout: PositiveFloat = 60
    # compute progress
    compute_progress_timeout: PositiveFloat = 60
    # tcp server sys call timeout
    tcp_server_timeout: PositiveFloat = 60
    # tcp server connect timeout
    tcp_server_connect_timeout: PositiveFloat = 120
    # generic distributed app, system limit timeout default
    remote_app_system_limit_timeout_default: PositiveFloat = 120
    # generic distributed app, death timeout default
    remote_app_death_timeout_default: PositiveFloat = 600
    # remote app, application processor timeout default
    remote_app_application_processor_timeout_default: PositiveFloat = 43200
    # remote app, queue default
    remote_app_queue_default: str = 'sequentiel'
    # remote app, workers per job array default
    remote_app_workers_per_job_array_default: PositiveInt = 10
    # remote app, log chroot
    # TODO : upgrade to pathlib
    remote_app_log_chroot_default: str = tempfile.mkdtemp(prefix='jobard_daemon')
    # maximum number of retries permitted on job array failure
    job_array_retries_max_nb: int = 0


class JobardCleanupExecutorConfig(JobardModel):
    """JobardCleanupExecutorConfig class."""

    # update missing cancel transition timeout
    update_missing_cancel_transition_timeout: PositiveFloat = 30
    # purge job order interval
    purge_job_order_interval: str = '30 days'
    # purge job order timeout
    purge_job_order_timeout: PositiveFloat = 1000


class JobardHealthCheckExecutorConfig(JobardModel):
    """JobardHealthCheckExecutorConfig class."""

    # healthcheck executor periodicity in seconds,
    # MUST BE lower than the lowest remote connection healthcheck periodicity
    healthcheck_executor_interval: PositiveFloat = 600

    # job_order table insert timeout
    insert_job_order_timeout: PositiveFloat = 20

    # select remote connections to test
    select_remote_connections_timeout: PositiveFloat = 20

    # select test image
    select_test_image_timeout: PositiveFloat = 20


class JobardExecutorConfig(JobardModel):
    """JobardExecutorConfig class."""

    # parser executor config
    parser: JobardParserExecutorConfig = JobardParserExecutorConfig()

    # processor executor config
    processor: JobardProcessorExecutorConfig = JobardProcessorExecutorConfig()

    # cleanup executor config
    cleanup: JobardCleanupExecutorConfig = JobardCleanupExecutorConfig()

    # healthcheck executor config
    healthcheck: JobardHealthCheckExecutorConfig = JobardHealthCheckExecutorConfig()


class JobardHealthCheckConfig(JobardModel):
    """JobardHealthCheckConfig class."""
    # maximum amount of memory to use (unit : K or M or G, default: 512M) for the healthcheck
    memory: Optional[Memory] = '512M'
    # maximum amount of CPU time to use (format: HH:MM:SS, default: 01:00:00) for the healthcheck
    walltime: Optional[Walltime] = '00:05:00'
    # command executed on the target cluster for the healthcheck
    command: str = 'echo 1'
    # extra configuration of the job (multiple values allowed) for the healthcheck
    job_extra: Optional[List[str]] = None


class JobardHostConfig(JobardModel):
    """JobardHostConfig class."""

    # min/base remote port for TCP exchange (WARN : must be unique per DAEMON instance, and not used by other app)
    base_remote_port: PositiveInt = 10802
    # hostname
    host: str
    # cluster type
    cluster: Optional[Cluster] = Cluster.LOCAL

    @validator("cluster", pre=True)
    def uppercase_strings(cls, value):
        """
        Converts string field to uppercase.
        Args:
            value: value to convert
        Returns:
            The uppercase value.
        """
        if isinstance(value, str):
            return value.upper()
        return value

    # port
    port: PositiveInt = 22
    # health check config
    healthcheck: JobardHealthCheckConfig = JobardHealthCheckConfig()
    # host connect timeout
    connect_timeout: PositiveInt = 30


class JobardDaemonSettings(JobardSettings):
    """Jobard Daemon system config."""

    # database config
    database: JobardDatabaseConfig = JobardDatabaseConfig()

    # core config
    core: JobardCoreConfig = JobardCoreConfig()

    # executors config
    executors: JobardExecutorConfig = JobardExecutorConfig()

    # ssh hosts config
    hosts_connections: Dict[str, JobardHostConfig] = {}

    # workspace
    workspace: Path = tempfile.mkdtemp(prefix='jobard')

    # purge connection table timeout
    purge_connection_timeout: PositiveFloat = 20

    # insert connection timeout
    insert_connection_timeout: PositiveFloat = 20

    # AES key file for ssh passwords and keys decryption
    aes_key_file: Path = None

    def check_host_connections(self):
        """
        If host connections is empty, add a default localhost connection.
        """
        if self.hosts_connections == {}:
            self.hosts_connections = {'localhost': JobardHostConfig(host='localhost')}


def load_daemon_sys_config(
    configuration_file: Path = None
) -> JobardDaemonSettings:
    """Load Daemon system config."""

    config = JobardDaemonSettings()
    if configuration_file:
        with open(configuration_file) as fd:
            config = config.merge(yaml.safe_load(fd))
    config.check_host_connections()
    return config


def parse_job_extra_conf(conf_extra):
    """Parse extra conf"""

    if conf_extra is None:
        job_extra = {}
    else:
        job_extra = list(filter(lambda x: '=' in x, conf_extra))
        keys = list(map(lambda x: x.split('=')[0], job_extra))
        values = list(map(lambda x: x.split('=')[1], job_extra))
        job_extra = dict(zip(keys, values))

    return job_extra


def get_str_conf_from_dict(conf_dict, name: str, default_value: str) -> str:
    """Get str from dict."""

    names = [name, name.replace('.', '_'), name.replace('.', '-')]
    for n in names:
        if n in conf_dict:
            return conf_dict[n]

    return default_value


def get_int_conf_from_dict(conf_dict, name: str, default_value: int) -> int:
    """Get conf from dict."""

    names = [name, name.replace('.', '_'), name.replace('.', '-')]
    for n in names:
        if n in conf_dict:
            return int(conf_dict[n])

    return default_value


def get_image_conf_from_dict(conf_dict):

    if 'image' in conf_dict:
        return conf_dict['image']

    return None
