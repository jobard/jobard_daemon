"""State type."""
from enum import Enum


class State(str, Enum):
    """Execution states."""

    INIT_ASKED = 'init_asked'
    INIT_RUNNING = 'init_running'
    NEW = 'new'
    ENQUEUED = 'enqueued'
    SUBMITTED = 'submitted'
    RUNNING = 'running'
    SUCCEEDED = 'succeeded'
    FAILED = 'failed'
    CANCELLATION_ASKED = 'cancellation_asked'
    CANCELLED = 'cancelled '
