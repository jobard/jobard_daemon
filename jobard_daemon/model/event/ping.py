"""PingEvent class."""
from jobard_daemon.model.base import JobardModel


class PingEvent(JobardModel):
    """PingEvent class."""

    message: str                                           # message to transmit
