"""Dask Over SSH Asynchronous Application Processor"""
import asyncio
import pathlib
from datetime import datetime, timedelta
from pathlib import Path

import asyncssh
from asyncssh import import_private_key

from jobard_daemon.core.exceptions import JobardError
from jobard_daemon.model.comm.ssh import RemoteSSHConfig
from jobard_daemon.model.comm.tunnel import ReverseTunnelConfig
from jobard_daemon.tcp.server import TCPServer
from jobard_daemon.utils.human import parse_human_size
from jobard_daemon.utils.job_logs import build_job_array_log_file
from jobard_daemon.utils.walltime import walltime_to_seconds


class DaskOverSSHApplicationProcessor:
    """Dask Over SSH Asynchronous Application Processor"""

    def __init__(
            self,
            loop,
            daemon_id: int,
            executor_id: int,
            remote_host_conf: RemoteSSHConfig,
            comm_tcp_server: TCPServer,
            tunnel_conf: ReverseTunnelConfig,
            daemon_log_output: Path,
            cancel_check_callback=None,
            logger=None,
    ):
        """Configure the Application Processor"""

        self._logger = logger

        self.loop = loop

        self.timed_out = False

        self.ssh_conn = None
        self.forward_listener = None

        self.tunnel_conf = tunnel_conf
        self.comm_tcp_server = comm_tcp_server
        self.remote_host_conf = remote_host_conf

        self.daemon_id = daemon_id
        self.executor_id = executor_id
        self.internal_name = str(self.daemon_id) + '_' + str(self.executor_id)

        # cancel callback
        self.cancel_check_callback = cancel_check_callback

        # asyncio task
        self.task = None

        # asyncio event
        self.end_event = None
        self.ssh_ready_event = None
        self.tcp_ready_event = None

        # build arguments for the ssh connection
        self.ssh_conn_arguments = {
            'host': self.remote_host_conf.host,
            'port': self.remote_host_conf.port,
            'username': self.remote_host_conf.username,
            'known_hosts': None,
        }
        if self.remote_host_conf.password is not None:
            self.ssh_conn_arguments['password'] = self.remote_host_conf.password.get_secret_value()
        elif self.remote_host_conf.client_keys is not None:
            self.ssh_conn_arguments['client_keys'] = list()
            for ssh_key in self.remote_host_conf.client_keys:
                self.ssh_conn_arguments['client_keys'].append(import_private_key(ssh_key))

        # Set debug level for SSH connection
        #asyncssh.set_debug_level(2)

        # daemon log output
        self.daemon_log_output = daemon_log_output

        super().__init__()

    def create_events(self):
        """Create events."""

        self.end_event = asyncio.Event(loop=self.loop)
        self.ssh_ready_event = asyncio.Event(loop=self.loop)
        self.tcp_ready_event = asyncio.Event(loop=self.loop)

    def process(self):
        """Process the distributed application"""

        self.create_events()
        self.loop.run_until_complete(self.kill())
        self.loop.run_until_complete(self.gather())

    def get_pid_filename(self):
        """Build the filename containing the pid of the remote dask process"""
        return f'/tmp/jobard_{self.internal_name}_{self.remote_host_conf.username}'

    async def async_process(self):
        """Process the distributed application"""

        self.create_events()
        await self.kill()
        await self.gather()

    async def kill(self):

        try:

            # Try to do the SSH connection within the connect_timeout defined
            conn = await asyncio.wait_for(
                asyncssh.connect(**self.ssh_conn_arguments),
                timeout=self.remote_host_conf.connect_timeout
            )

            # with the SSH connection ..
            async with conn:

                # .. build the command. and sleep a bit, in order to reuse port
                pid_filename = self.get_pid_filename()
                command = "/bin/bash -c 'if test -f "+pid_filename + \
                          '; then kill -9 `cat '+pid_filename+'`' + \
                          '; rm '+pid_filename+"; sleep 10; fi'"

                # .. check and try to kill any previous instance running
                await asyncio.wait_for(conn.run(command), timeout=self.remote_host_conf.timeout)

        except Exception as error:
            raise JobardError(
                'unable to connect to ' + self.remote_host_conf.host + ', in order to cleanup previous execution'
            )

    # TODO : refactor
    async def async_cleanup(self):
        """Cleanup after executing"""

        self.cleanup_requested()

        if self.forward_listener is not None:
            try:
                self.forward_listener.close()
            except Exception:
                pass

        if self.ssh_conn is not None:
            try:
                self.ssh_conn.close()
            except Exception:
                pass

        await self.kill()

    def cleanup(self):
        """Cleanup after executing"""

        self.cleanup_requested()

        if self.forward_listener is not None:
            try:
                self.forward_listener.close()
            except Exception:
                pass

        if self.ssh_conn is not None:
            try:
                self.ssh_conn.close()
            except Exception:
                pass

        self.loop.run_until_complete(self.kill())

    def cleanup_requested(self):
        """Cleanup is request."""

        # unlock coroutines
        if self.end_event is not None:
            self.end_event.set()
            self.ssh_ready_event.set()
            self.tcp_ready_event.set()

    async def gather(self):
        """Wait on multiples coroutines implied in the processing"""

        await asyncio.gather(
            self.timeout(),
            self.tcp_server(),
            self.ssh_reverse_tunnel(),
            self.core(),
            self.cancel(),
        )

    async def timeout(self) -> None:
        """Prevent the processor of running indefinitely in case of unknown errors"""

        i = 0
        recent_heartbeat_received = True
        timeout = walltime_to_seconds(
            self.comm_tcp_server.app.walltime) + self.comm_tcp_server.app.application_processor_timeout
        while i < timeout:
            i = i + 1
            await asyncio.sleep(1)
            if self.end_event.is_set():
                break

            plus_delta = self.comm_tcp_server.last_ping + \
                timedelta(seconds=float(self.comm_tcp_server.app.death_timeout))

            if plus_delta < datetime.now():
                recent_heartbeat_received = False
                break

        if recent_heartbeat_received is False or self.end_event.is_set() is False:
            self.timed_out = True

        if self.task is not None:
            if self.task.cancelled is False:
                self.task.cancel()

        self.cleanup_requested()

    async def ssh_reverse_tunnel(self) -> None:
        """Create and bind the reverse SSH Tunnel required to the app processing"""

        try:

            # Try to do the SSH connection within the connect_timeout defined
            conn = await asyncio.wait_for(
                asyncssh.connect(**self.ssh_conn_arguments),
                timeout=self.remote_host_conf.connect_timeout
            )

            # with the SSH connection ..
            async with conn:
                self.ssh_conn = conn

                # .. create a port forwarding, for tcp exchange between the jobard daemon, and a remote app
                forward_listener = await asyncio.wait_for(conn.forward_remote_port(
                    self.tunnel_conf.remote_host,
                    self.tunnel_conf.remote_port,
                    self.tunnel_conf.local_host,
                    self.tunnel_conf.local_port
                ), timeout=self.remote_host_conf.timeout)
                self.forward_listener = forward_listener

                # .. signal that SSH connection is ready
                self.ssh_ready_event.set()

                # .. leave the SSH connection open, until we decide to close it
                await self.end_event.wait()

                # .. free up port forwarding
                forward_listener.close()
                self.forward_listener = None
                await asyncio.wait_for(forward_listener.wait_closed(), self.remote_host_conf.timeout)

        except (asyncio.TimeoutError, asyncio.CancelledError, JobardError):
            raise JobardError(
                'unable to connect to ' + self.remote_host_conf.host + ' and bind the associated tunnel'
            )
        finally:
            self.cleanup_requested()
            self.ssh_conn = None

    async def tcp_server(self) -> None:
        """Run the TCP Server needed for communication back to the daemon"""

        self.comm_tcp_server.end_event = self.end_event
        self.comm_tcp_server.ready_event = self.tcp_ready_event
        await self.comm_tcp_server.run()

    async def cancel(self) -> None:
        """Trigger cancel check callback"""

        await self.ssh_ready_event.wait()
        await self.tcp_ready_event.wait()

        if self.cancel_check_callback is not None:
            while not self.end_event.is_set():
                # TODO : set in yaml system file
                await asyncio.sleep(10)
                cancel_events = await self.cancel_check_callback()
                for cancel_event in cancel_events:
                    self.comm_tcp_server.add_job_to_cancel(cancel_event)

    def log_file(self):
        """Log file."""
        return build_job_array_log_file(
            daemon_log_output=self.daemon_log_output,
            cluster_type=str(self.comm_tcp_server.app.cluster_type.value),
            daemon_id=self.daemon_id,
            executor_id=self.executor_id,
            job_order_id=self.comm_tcp_server.app.job_order_id,
            job_array_id=self.comm_tcp_server.app.job_array_id,
            try_id=self.comm_tcp_server.app.try_id
        )

    async def core(self) -> None:
        """Trigger the application to run effectively through SSH"""

        try:

            await self.ssh_ready_event.wait()
            await self.tcp_ready_event.wait()

            if self.end_event.is_set() is True:
                return

            log_file = self.log_file()

            # prepare the remote command. and try to adjust any cpu/ram limit restrictions
            cpu_time = \
                walltime_to_seconds(
                    self.comm_tcp_server.app.walltime
                ) + self.comm_tcp_server.app.system_limit_timeout

            command = '{0} {1}{2} {3} {4}'.format(
                self.remote_host_conf.entry_point_wrapper.as_posix() if self.remote_host_conf.entry_point_wrapper else '',
                self.remote_host_conf.app_path.as_posix() + pathlib.os.sep if self.remote_host_conf.app_path else '',
                self.remote_host_conf.entry_point.as_posix(),
                self.tunnel_conf.remote_host,
                self.tunnel_conf.remote_port
            ).strip()

            wrapped_command = \
                "/bin/bash -c '" + \
                ' echo $$ > '+self.get_pid_filename() + \
                ' && mkdir -p ' + self.daemon_log_output.as_posix() + pathlib.os.sep + \
                ' && ulimit -t ' + str(cpu_time) + \
                ' && ulimit -m ' + str(int(parse_human_size(self.comm_tcp_server.app.driver_memory) // 1000)) + \
                ' && cd ' + self.remote_host_conf.cwd_path.as_posix() + pathlib.os.sep + \
                ' && ' + command + ' &> ' + log_file.as_posix() + "'"

            # run the remote application
            self.task = asyncio.create_task(self.ssh_conn.run(wrapped_command))
            result = await self.task

            # do log command std
            self._logger.debug(result.stdout)
            self._logger.debug(result.stderr)

            if result.exit_status != 0:
                raise JobardError(
                    'the application ' + self.comm_tcp_server.app.name +
                    ' have failed (status_code = ' + str(result.exit_status) + ')'
                )

        except asyncio.CancelledError:
            raise JobardError('the application ' + self.comm_tcp_server.app.name + ' has been cancelled')
        finally:
            self.cleanup_requested()
