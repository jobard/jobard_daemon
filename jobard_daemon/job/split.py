"""Utilities to distributes jobs in array."""
from typing import List, Optional, Tuple

from jobard_daemon.type.basic import Split


def split(jobs: List[Tuple[List[str], List[str]]], split_required: Optional[Split] = None) -> \
        List[List[Tuple[List[str], List[str]]]]:
    """Split jobs (command, args) in arrays"""

    if split_required is None:
        job_arrays = [jobs]
    elif isinstance(split_required, str) and split_required[0] == '/':
        n = int(split_required[1:])
        x, y = divmod(len(jobs), n)
        tmp = list((jobs[i * x + min(i, y):(i + 1) * x + min(i + 1, y)] for i in range(n)))
        job_arrays = [ll for ll in tmp if len(ll) > 0]
    else:
        n = int(split_required)
        job_arrays = [jobs[i:i + n] for i in range(0, len(jobs), n)]

    return job_arrays
