"""Remote Host Config class."""
from pathlib import Path

from pydantic.types import SecretStr

from jobard_daemon.model.base import JobardModel


class RemoteSSHConfig(JobardModel):
    """Remote SSH Config class."""

    host: str                               # host
    port: int = 22                          # port
    connect_timeout = 10                    # connect time out
    timeout = 20                            # time out
    username: str                           # username
    password: SecretStr = None              # password
    entry_point_wrapper: Path = None        # python path (/bin/python). it could be a python binary into a conda env.
    app_path: Path = None                   # an app directory, that contains at least the Jobard ./run/app scripts.
    entry_point: Path = None                # the entry point (script), into the previous directory
    cwd_path: Path = None                   # initial cwd
    client_keys: list[str] = None           # alternatively, use public key authentication
