"""Jobard Tcp Server class"""
import asyncio
from asyncio import Event
from datetime import datetime
from typing import Callable, List

from jobard_daemon.core.exceptions import JobardError
from jobard_daemon.model.event.app import AppEvent
from jobard_daemon.model.event.cancel import JobCancelEvent
from jobard_daemon.model.event.job import JobEvent
from jobard_daemon.model.event.ping import PingEvent
from jobard_daemon.model.event.progress import JobProgressEvent
from jobard_daemon.model.event.request import RequestHeaderEvent
from jobard_daemon.model.event.response import ResponseHeaderEvent
from jobard_daemon.tcp.base import TCPBase
from jobard_daemon.tcp.protocol import JProtocol
from jobard_daemon.type.op import Op


class TCPServer(TCPBase):
    """Jobard Tcp Server class"""

    def __init__(
            self,
            loop,
            timeout: int,
            connect_timeout: int,
            host: str,
            port: int,
            app: AppEvent,
            jobs: list[JobEvent],
            progress_callback: Callable[[List[JobProgressEvent]], None],
            cancel_callback: Callable[[List[JobCancelEvent]], None],
            async_progress_callback=None,
            async_cancel_callback=None,
            end_event: Event = None,
            ready_event: Event = None,
    ):
        """Configure the TCP server"""

        self.last_ping = datetime.now()
        self.server = None
        self.host = host
        self.port = port
        self.app = app
        self.jobs = jobs
        self.loop = loop
        self.end_event = end_event
        self.ready_event = ready_event
        self.progress_callback = progress_callback
        self.cancel_callback = cancel_callback
        self.jobs_to_cancel: List[JobCancelEvent] = []

        self.async_progress_callback = async_progress_callback
        self.async_cancel_callback = async_cancel_callback

        super().__init__(timeout, connect_timeout)

    def add_job_to_cancel(self, cancel: JobCancelEvent):
        self.jobs_to_cancel.append(cancel)
        self.jobs_to_cancel: List[JobCancelEvent] = list(set(self.jobs_to_cancel))

    async def run(self):
        """Run the TCP server"""

        try:
            self.server = await asyncio.wait_for(
                asyncio.start_server(self.handle, host=self.host, port=self.port, loop=self.loop),
                timeout=self.connect_timeout
            )

            self.ready_event.set()
            await self.end_event.wait()

            self.server.close()
            await asyncio.wait_for(self.server.wait_closed(), self.timeout)
        except (asyncio.TimeoutError, asyncio.CancelledError):
            raise JobardError('run_server : unable to bind to ' + self.host + ':' + str(self.port) + ' port (timeout)')
        finally:
            self.ready_event.set()

    async def handle(self, reader, writer):
        """Handle a new connection to the TCP server"""

        extra_infos = self.tcp_extra(writer)

        ops = {
            Op.PING: self.op_ping,
            Op.GET_APP: self.op_get_app,
            Op.GET_COMMANDS: self.op_get_commands,
            Op.JOB_REPORT_PROGRESS: self.op_job_report_progress,
            Op.JOB_CANCEL: self.op_job_cancel,
        }

        try:

            # read request header
            request_header = await self.read_object(reader=reader, extra_infos=extra_infos)
            request_header_obj = JProtocol.decode_request_header(request_header)

            await ops[request_header_obj.op](reader, writer, request_header_obj, extra_infos)

        finally:
            await self.close_writer(writer)

    async def op_ping(self, reader, writer, request_header: RequestHeaderEvent, extra_infos):
        """Handle a Ping Op"""

        # write response header
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_response_header(ResponseHeaderEvent(obj_count=1)),
            extra_infos=extra_infos
        )

        # write response body
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_ping_event(PingEvent(message='pong')),
            extra_infos=extra_infos
        )

        self.last_ping = datetime.now()

    async def op_get_app(self, reader, writer, request_header: RequestHeaderEvent, extra_infos):
        """Handle a Get App Op"""
        # write response header
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_response_header(ResponseHeaderEvent(obj_count=1)),
            extra_infos=extra_infos
        )

        # write response body
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_app_event(self.app),
            extra_infos=extra_infos
        )

    async def op_get_commands(self, reader, writer, request_header: RequestHeaderEvent, extra_infos):
        """Handle a Get Commands Op"""

        # write response header
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_response_header(ResponseHeaderEvent(obj_count=len(self.jobs))),
            extra_infos=extra_infos
        )

        # write response body
        jobs_len = len(self.jobs)
        for i in range(0, jobs_len):
            await self.write_object(
                writer=writer,
                data=JProtocol.encode_job_event(self.jobs[i]),
                extra_infos=extra_infos
            )

    async def op_job_report_progress(self, reader, writer, request_header: RequestHeaderEvent, extra_infos):
        """Handle a Job Report Progress Op"""

        # process request body
        jobs_progress = []
        jobs_len = request_header.obj_count
        for i in range(0, jobs_len):
            tmp = await self.read_object(reader=reader, extra_infos=extra_infos)
            job_progress = JProtocol.decode_job_progress_event(tmp)
            jobs_progress.append(job_progress)

        # write response header
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_response_header(ResponseHeaderEvent(obj_count=0)),
            extra_infos=extra_infos
        )

        # close writer now to free-up underlying stream ASAP
        await self.close_writer(writer)

        # call the progress callback
        if self.async_progress_callback is not None:
            await self.async_progress_callback(jobs_progress)
        else:
            self.progress_callback(jobs_progress)

    async def op_job_cancel(self, reader, writer, request_header: RequestHeaderEvent, extra_infos):
        """Handle a Job Cancel Op"""

        cancels = self.jobs_to_cancel

        # write response header
        await self.write_object(
            writer=writer,
            data=JProtocol.encode_response_header(ResponseHeaderEvent(obj_count=len(cancels))),
            extra_infos=extra_infos
        )

        # write response body
        jobs_len = len(cancels)
        for i in range(0, jobs_len):
            await self.write_object(
                writer=writer,
                data=JProtocol.encode_job_cancel_event(cancels[i]),
                extra_infos=extra_infos
            )

        # close writer now to free-up underlying stream ASAP
        await self.close_writer(writer)

        # purge the list of jobs to cancel
        self.jobs_to_cancel: List[JobCancelEvent] = list(set(self.jobs_to_cancel) - set(cancels))

        # trigger the callback, with the job we assumed to be cancelled
        if self.async_cancel_callback is not None:
            await self.async_cancel_callback(cancels)
        else:
            self.cancel_callback(cancels)
