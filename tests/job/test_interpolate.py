"""Test command interpolating utilities."""
import os
import shlex
from typing import List, Dict

import pandas as pd
from jinja2 import Template, UndefinedError

import pytest

from jobard_daemon.job.interpolate import interpolate


@pytest.mark.parametrize('command, arguments', [
    (
        'echo {{ arg[0] }} {{ arg[1] }}',
        {'arg': ['File', 'toto.nc']},
    ),
])
def test_interpolate_command(command: str, arguments: Dict[str, List[str]]):

    j2_template = Template(command)
    print(j2_template.render(arguments))


@pytest.mark.parametrize('command, expected', [
    (
        "echo 'ma chaine avec espace' '{{ arg[0] }}' file '{{ arg[1] }}'", 5
    ),
    (
        "echo dataset_id '{{ arg[0] }}' file '{{ arg[1] }}'", 5
    ),
    (
        "/home1/bin/felyx-extraction -c /home1/felyx_data_maxss.yaml --dataset_id '{{ arg[0] }}' "
        '--miniprod_dir /home1/data/ '
        '--manifest_dir /home1//manifests/ '
        "inputs '{{ arg[1] }}'", 11
    ),
])
def test_interpolate(command: str, expected: int):
    # read the arguments list
    arg_array = list()
    df = pd.read_csv(os.path.dirname(__file__) + '/command_files.txt', header=None, delimiter=' ')
    for index, row in df.iterrows():
        arguments = list()
        for arg in row.values:
            arguments.append(arg)
        arg_array.append(arguments)
    jobs_resolved = interpolate(command=shlex.split(command), arguments=arg_array)
    print('\n')
    print(jobs_resolved)

    assert len(jobs_resolved) == 16
    for job in jobs_resolved:
        assert len(job[0]) == expected


@pytest.mark.parametrize('command, expected', [
    (
        "echo dataset_id '{{ arg[0] }}' file '{{ arg[2] }}'",
        UndefinedError
    ),
    (
        "echo dataset_id '{{ toto[0] }}' file '{{ arg[0] }}'",
        UndefinedError
    ),
])
def test_interpolate_error(command: str, expected: Exception):
    # read the arguments list
    arg_array = list()
    df = pd.read_csv(os.path.dirname(__file__) + '/command_files.txt', header=None, delimiter=' ')
    for index, row in df.iterrows():
        arguments = list()
        for arg in row.values:
            arguments.append(arg)
        arg_array.append(arguments)

    # interpolate the command
    with pytest.raises(expected):
        interpolate(command=shlex.split(command), arguments=arg_array)

