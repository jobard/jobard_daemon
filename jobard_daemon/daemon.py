"""Jobard API client."""
import asyncio
import concurrent
import logging
import time

import asyncpg

from jobard_daemon.core.exceptions import JobardError
from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.run.executor.cleanup import CleanupExecutor
from jobard_daemon.run.executor.healthcheck import HealthCheckExecutor
from jobard_daemon.run.executor.parse import JobOrderParserExecutor
from jobard_daemon.run.executor.process import JobOrderJobArrayExecutor
from jobard_daemon.utils.configuration import JobardDaemonSettings

logger = logging.getLogger('jobard_daemon')


def daemon_app_handle_exception(daemon, log):
    """Handle daemon unhandled exception."""

    def internal_handle_exception(loop, context):  # this function is a callback. please keep the unused "loop" arg.
        msg = context.get('exception', context['message'])
        exc_context = 'Handled exception from asyncio loop {0}'.format(context)
        exc_msg = f'Caught exception: {msg}'

        log.warn(exc_context)
        log.warn(exc_msg)

        # TODO : trigger method from the "daemon" here, if needed, regarding the exception

    return internal_handle_exception


class JobardDaemon(object):
    """Jobard daemon class."""

    PARSE_JOB_ORDER_EXECUTOR_IDLE_INTERVAL = 10
    PROCESS_JOB_ARRAY_EXECUTOR_IDLE_INTERVAL = 10
    CLEANUP_EXECUTOR_IDLE_INTERVAL = 60

    def __init__(
        self,
        settings: JobardDaemonSettings,
        log: JobardLogger,
    ):
        """Initialize class.

        Args:
            settings: JobardSettings
        """
        self._logger = log
        self._settings = settings or JobardDaemonSettings()

    @staticmethod
    def process_executor(
        i: int,
        config: JobardDaemonSettings,
        log_level: int,
    ):
        """Create a dedicated loop for the job array process executor."""

        # job array processor executor log init
        _logger = JobardLogger(level=log_level)
        _logger.create(config.core.daemon_id, 'JAPE' + str(i).zfill(2))
        _logger.info('initializing Job Array Processor executor (JAPE) ..')

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        while True:
            res = None
            try:
                res = loop.run_until_complete(
                    JobOrderJobArrayExecutor.async_process_job_array(
                        loop, i, config, _logger
                    )
                )
            except Exception as error:
                _logger.warn('an error occurred')
                _logger.exception(error)
            if res is None or res is False:
                # wait a bit if we have not enough work, or if an exception occurred
                time.sleep(JobardDaemon.PROCESS_JOB_ARRAY_EXECUTOR_IDLE_INTERVAL)

        # TODO : graceful shutdown
        loop.close()

    @staticmethod
    def parse_executor(
        i: int,
        config: JobardDaemonSettings,
        log_level: int,
    ):
        """Create a dedicated loop for the job order parser executor."""

        # parser executor log init
        _logger = JobardLogger(level=log_level)
        _logger.create(config.core.daemon_id, 'PE' + str(i).zfill(4))
        _logger.info('initializing Parser executor (PE) ..')

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        while True:
            res = None
            try:
                res = loop.run_until_complete(
                    JobOrderParserExecutor.async_parse_job_orders(loop, i, config, _logger)
                )
            except Exception as error:
                _logger.warn('an error occurred')
                _logger.exception(error)
            if res is None or res is False:
                # wait a bit if we have not enough work, or if an exception occurred
                _logger.info('sleeping a bit')
                time.sleep(JobardDaemon.PARSE_JOB_ORDER_EXECUTOR_IDLE_INTERVAL)

        # TODO : graceful shutdown
        loop.close()

    @staticmethod
    def cleanup_executor(
        i: int,
        config: JobardDaemonSettings,
        log_level: int,
    ):
        """Create a dedicated loop for the cleanup executor."""

        # parser executor log init
        _logger = JobardLogger(level=log_level)
        _logger.create(config.core.daemon_id, 'CE' + str(i).zfill(4))
        _logger.info('initializing Cleanup executor (CE) ..')

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        while True:
            try:
                loop.run_until_complete(
                    CleanupExecutor.async_cleanup(loop, i, config, _logger)
                )
            except Exception as error:
                _logger.warn('an error occurred')
                _logger.exception(error)
            # wait a bit
            _logger.info('sleeping a bit')
            time.sleep(JobardDaemon.CLEANUP_EXECUTOR_IDLE_INTERVAL)

        # TODO : graceful shutdown
        loop.close()

    @staticmethod
    def healthcheck_executor(
            i: int,
            config: JobardDaemonSettings,
            log_level: int,
    ):
        """Create a dedicated loop for the jobard daemon healthcheck executor."""
        # healthcheck executor log init
        _logger = JobardLogger(level=log_level)
        _logger.create(config.core.daemon_id, 'JDHE' + str(i).zfill(2))
        _logger.info('initializing Jobard Daemon Healthcheck Executor (JDHE) ..')

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        while True:
            start_time = time.time()
            try:
                loop.run_until_complete(
                    HealthCheckExecutor.async_health_check(
                        loop, i, config, _logger
                    )
                )
            except Exception as error:
                _logger.warn('an error occurred')
                _logger.exception(error)

            # Sleeping a bit if time remains before the next health check.
            end_time = time.time()
            if end_time - start_time < config.executors.healthcheck.healthcheck_executor_interval:
                sleep_duration = (start_time+config.executors.healthcheck.healthcheck_executor_interval)-end_time
                _logger.info('healthcheck is sleeping for : {} seconds'.format(round(sleep_duration)))
                time.sleep(sleep_duration)

        # TODO : graceful shutdown
        loop.close()

    async def async_run(
            self,
            loop,
            config: JobardDaemonSettings,
    ):
        """Run every coroutines in separate processes."""

        # set asyncio loop exception handler
        loop.set_exception_handler(daemon_app_handle_exception(self, self._logger))

        # build a list of executors
        executors_list = []

        # add "healthcheck" executor
        executors_list.extend([[
            self.healthcheck_executor,
            i,
            config,
            self._logger.level,
        ] for i in range(config.core.nb_healthcheck_executors)]
        )

        # add "parse_job_orders" executors
        executors_list.extend([
            [
                self.parse_executor,
                i,
                config,
                self._logger.level,
            ] for i in range(config.core.nb_parser_executors)]
        )

        # add "process_job_array" executors
        executors_list.extend([
            [
                self.process_executor,
                i,
                config,
                self._logger.level,
            ] for i in range(config.core.nb_processor_executors)]
        )

        # add "cleanup" executors
        executors_list.extend([
            [
                self.cleanup_executor,
                i,
                config,
                self._logger.level,
            ] for i in range(config.core.nb_cleanup_executors)]
        )

        # calculate the number of workers to set max_workers on the ProcessPoolExecutor, and maximize parallelism
        max_workers = len(executors_list)

        # create the Process Pool Executor, and run every coroutine associated to a given process
        with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
            return await asyncio.gather(
                *[loop.run_in_executor(executor, p[0], p[1], p[2], p[3]) for p in executors_list]
            )

    async def pre_actions(
            self,
            loop,
            config: JobardDaemonSettings,
    ):
        """Do pre actions."""

        # Read the AES key from the path given in the daemon configuration
        try:
            with open(config.aes_key_file) as f:
                aes_key = f.read().strip()
        except Exception as e:
            raise JobardError(f'Error while reading AES key file: {e}')
        if not aes_key:
            raise JobardError('Missing AES key, check daemon configuration')
        JobOrderJobArrayExecutor.aes_key = aes_key

        # try to connect to the database, until a max retries number
        conn = None
        max_retries = config.database.connect_max_retries
        retries_nb = 0
        connected = False
        while retries_nb < max_retries and connected is False:
            try:
                self._logger.info(
                    'Trying to connect to the database, try = ' + str(retries_nb+1) + '/' + str(max_retries)
                )
                if conn is None:
                    conn = await asyncpg.connect(
                        dsn=config.database.dsn.format(),
                        loop=loop,
                        timeout=config.database.connect_timeout,
                        server_settings={
                            'application_name': 'jobard - pre_actions - daemon=' + str(config.core.daemon_id)
                        }
                    )

                # execute a test request
                await conn.execute(
                    'SELECT 1',
                    timeout=config.database.default_timeout
                )
                connected = True
                self._logger.info('Successfully connected to the database')

            except Exception as error:
                retries_nb = retries_nb + 1
                if retries_nb == max_retries:
                    raise error
                else:
                    time.sleep(config.database.connect_timeout)

        try:

            # Reinitialize job states and concurrency

            # TODO : the daemon_id must be taken into account in the following requests

            # reset concurrency (no daemon running yet)
            self._logger.info('reset wrong concurrency')
            await conn.execute(
                'UPDATE "job_order"'
                ' SET "concurrency" = 0',
                timeout=config.database.default_timeout
            )
            self._logger.info('done')

            # reset wrong job order states (no daemon running yet)
            self._logger.info('reset wrong states')
            for table in ['job', 'job_order', 'job_array']:
                await conn.execute(
                    'UPDATE "' + table + '" '
                    ' SET "state" = \'FAILED\''
                    ' WHERE "state" = \'ENQUEUED\' OR'
                    ' "state" = \'SUBMITTED\' OR'
                    ' "state" = \'NEW\' OR'
                    ' "state" = \'INIT_RUNNING\' OR'
                    ' "state" = \'RUNNING\' OR'
                    ' "state" = \'CANCELLATION_ASKED\'',
                    timeout=config.database.default_timeout
                )
            self._logger.info('done')

        finally:
            # Close the connection.
            await conn.close()

    def run(
            self,
    ):
        """Run The Jobard daemon."""

        self._logger.info('Start the daemon')

        # TODO : add async-sig / signal handler, or a mecanism to gracefully terminate all coroutines

        self._logger.info('Setup asyncio')

        # Create and set the main asyncio loop
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        self._logger.info('Do pre-actions')

        loop.run_until_complete(self.pre_actions(loop, self._settings))

        self._logger.info('Running now core asyncio gather')

        # Process the different tasks in separate processes, in order to mitigate cpu-bound code side effects
        try:
            loop.run_until_complete(self.async_run(
                loop,
                self._settings
            ))
        finally:
            # Close the loop
            loop.close()

        self._logger.info('End of the daemon')
