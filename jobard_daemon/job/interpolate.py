"""Utilities to interpolate command line."""
import shlex
from typing import List, Tuple
from jinja2 import Template, StrictUndefined


def interpolate(
        command: List[str],
        arguments: List[List[str]]) -> \
        List[Tuple[List[str], List[str]]]:
    """
    Interpolate command line (command, args) using jinja2.
    Args:
        command: command as a list of arguments.
        arguments: the values list for each argument of the arguments list.
    Returns:
        A list of interpolated commands.
    """
    command_interpolated = list()
    args_dict = dict()
    command_str = ' '.join(f"'{arg}'" for arg in command)
    j2_template = Template(command_str, undefined=StrictUndefined)
    for argument_array in arguments:
        args_dict['arg'] = argument_array
        rendered_data = j2_template.render(args_dict)
        # Use shlex split to preserve quotes around the arguments with spaces
        command_interpolated.append((shlex.split(rendered_data), argument_array))
    return command_interpolated
