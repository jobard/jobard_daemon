"""Jobard parser executor."""
import asyncpg

from jobard_daemon.core.exceptions import JobardError
from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.job.interpolate import interpolate
from jobard_daemon.job.split import split
from jobard_daemon.utils.configuration import JobardDaemonSettings

# global app-level postgresql lock for this type of executor
lock_parse_job_orders_id = 2040403040


class JobOrderParserExecutor:
    """Jobard parser executor."""

    @staticmethod
    async def async_parse_job_orders(
            loop,
            executor_id: int,
            config: JobardDaemonSettings,
            _logger: JobardLogger,
    ):

        _logger.info('start')

        # connect to the database
        conn = await asyncpg.connect(
            dsn=config.database.dsn.format(),
            loop=loop,
            timeout=config.database.connect_timeout,
            server_settings={
                'application_name': 'jobard - parse_job_orders - daemon=' +
                str(config.core.daemon_id) + ' executor=' + str(executor_id)
            }
        )

        id_to_process = 0
        try:

            # open the transaction
            async with conn.transaction(isolation='read_committed', readonly=False):

                # lock the transaction
                await conn.execute(
                    'SELECT pg_advisory_xact_lock(' + str(lock_parse_job_orders_id) + ');', timeout=60)

                # get a job order to init
                row = await conn.fetchrow(
                    'SELECT "id"'
                    ' FROM "job_order"'
                    ' WHERE "state" = \'INIT_ASKED\''
                    ' ORDER BY "priority" ASC'
                    ' LIMIT 1'
                    , timeout=config.executors.parser.lock_timeout
                )

                if row is not None:
                    id_to_process = int(row['id'])

                    # update
                    await conn.execute(
                        'UPDATE "job_order"'
                        ' SET "state" = \'INIT_RUNNING\''
                        ' WHERE "id" = $1 AND "state" = \'INIT_ASKED\'',
                        id_to_process
                        , timeout=config.executors.parser.update_job_order_timeout
                    )

            # process the id
            if id_to_process > 0:

                _logger.info(
                    'job order id = ' + str(id_to_process) + ', ' +
                    'now fetching full command arguments'
                )

                try:

                    # get full details
                    row = await conn.fetchrow(
                        'SELECT "command", "arguments", "interpolate_command", "split", "connection"'
                        ' FROM "job_order"'
                        ' WHERE "id" = $1'
                        ,
                        id_to_process
                        , timeout=config.executors.parser.select_job_order_arguments_timeout
                    )

                    # Retrieve parameters in configuration for the host connection
                    conn_name = row['connection']
                    if conn_name is None:
                        raise JobardError('host connection name is required')

                    if conn_name not in config.hosts_connections:
                        raise JobardError(
                            'Unable to find in the deamon configuration file the host connection with name : '
                            + conn_name)

                    if config.hosts_connections[conn_name] is None:
                        raise JobardError(
                            'Unable to read in tne daemon configuration file the host connection with name : '
                            + conn_name)

                    # resolve each command with their arguments
                    jobs_resolved = []
                    command = row['command']
                    arguments = row['arguments']
                    interpolate_command = row['interpolate_command']
                    split_required = row['split']

                    _logger.info(
                        'job order id = ' + str(id_to_process) + ', ' +
                        'now interpolating'
                    )

                    if interpolate_command is None or interpolate_command is False:
                        # simply append the argument to the command
                        for argument_array in arguments:
                            jobs_resolved.append((command, argument_array))
                    else:
                        jobs_resolved = interpolate(command=command, arguments=arguments)

                    _logger.info(
                        'job order id = ' + str(id_to_process) + ', ' +
                        'now splitting'
                    )

                    # split jobs in job_array
                    jobs_split = split(jobs_resolved, split_required)

                    # for each job array
                    i = 0
                    job_array_values = []
                    for job_array in jobs_split:
                        c = len(job_array)
                        job_array_values.append((id_to_process, i, c))
                        i += c

                    # check if we have at least one job ?
                    if len(job_array_values) == 0:
                        raise JobardError('no job to proceed')

                    # open the transaction
                    async with conn.transaction(isolation='read_committed', readonly=False):

                        # speed up the transaction by deferring integrity constraints
                        await conn.execute('SET CONSTRAINTS ALL DEFERRED;')

                        _logger.info(
                            'job order id = ' + str(id_to_process) + ', ' +
                            'dropping previous job array and job entities, if exist'
                        )

                        # drop any previous jobs arrays (and job by cascading)
                        await conn.execute(
                            'DELETE FROM "job_array" AS ja WHERE ja.job_order = $1'
                            , id_to_process
                            , timeout=config.executors.parser.delete_job_timeout
                        )

                        _logger.info(
                            'job order id = ' + str(id_to_process) + ', ' +
                            'bulding job array entities'
                        )

                        # insert job arrays
                        statement = """INSERT INTO job_array (job_order, job_offset, job_count) VALUES($1, $2, $3);"""
                        await conn.executemany(
                            statement,
                            job_array_values,
                            timeout=config.executors.parser.insert_many_job_array_timeout
                        )

                        # select back job array id
                        rows = await conn.fetch(
                            'SELECT "id"'
                            ' FROM "job_array" AS ja'
                            ' WHERE ja.job_order = $1'
                            ' ORDER BY "job_offset" ASC'
                            ,
                            id_to_process
                            , timeout=config.executors.parser.select_job_array_timeout
                        )

                        # get the list of job array' ids
                        ids = [row['id'] for row in rows]

                        # create jobs values to commit in the database
                        job_array_idx = 0
                        jobs_values = []
                        for job_array in jobs_split:
                            for job in job_array:
                                if interpolate_command is None or interpolate_command is False:
                                    jobs_values.append((ids[job_array_idx], job_array_idx, job[0] + job[1], job[1]))
                                else:
                                    # For interpolated command, args must not be concatenated with command
                                    jobs_values.append((ids[job_array_idx], job_array_idx, job[0], job[1]))
                            job_array_idx = job_array_idx + 1

                        _logger.info(
                            'job order id = ' + str(id_to_process) + ', ' +
                            'bulding job entities'
                        )

                        # commit the job values
                        statement = """INSERT
                                    INTO "job"
                                    (job_array, job_array_index, command_with_parameters, arguments)
                                    VALUES($1, $2, $3, $4);
                        """
                        await conn.executemany(
                            statement,
                            jobs_values,
                            timeout=config.executors.parser.insert_many_job_timeout
                        )

                        # success of the init (parsing the job order)
                        await conn.execute(
                            'UPDATE "job_order" SET "state" = \'NEW\', "job_count" = $1'
                            ' WHERE "id" = $2 AND "state" = \'INIT_RUNNING\'',
                            len(jobs_values),
                            id_to_process,
                            timeout=config.executors.parser.update_job_order_timeout
                        )

                        _logger.info(
                            'job order id = ' + str(id_to_process) + ', ' +
                            'commit all entities'
                        )

                except Exception as error:

                    _logger.warn(
                        'job order id = ' + str(id_to_process) + ', ' +
                        'an error occured'
                    )
                    _logger.exception(error)

                    # unable to create the entities
                    await conn.execute(
                        """
                        UPDATE "job_order" 
                        SET "state" = 'FAILED', "error_message" = CONCAT_WS('|',error_message, $2::text)
                        WHERE "id" = $1 AND "state" = 'INIT_RUNNING'
                        """,
                        id_to_process,
                        str(error),
                        timeout=config.executors.parser.update_job_order_timeout
                    )

                finally:
                    pass

            else:
                _logger.info(
                    'nothing to do'
                )

        finally:
            # Close the connection.
            await conn.close()

        _logger.info('end')

        # tell the caller that we have other work to do, so we can trigger another instance of this function immediately
        return id_to_process > 0
