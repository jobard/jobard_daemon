"""Signal management utilities."""
import signal


class GracefulInterruptHandler(object):
    """Signal handler context manager."""

    def __init__(self, sig=signal.SIGINT):
        """Signal handler constructor.

        Args:
            sig: signal
        """
        self.sig = sig
        self.released = False

    def __enter__(self):  # noqa: D105
        self.interrupted = False
        self.released = False
        self.original_handler = signal.getsignal(self.sig)
        signal.signal(self.sig, self._handle_signal)
        return self

    def __exit__(self, type, value, tb):  # noqa: D105, WPS125, WPS110
        self.release()

    def release(self):
        """Release handler.

        Returns:
            True if the signal handle is released
        """
        if self.released:
            return False
        signal.signal(self.sig, self.original_handler)
        self.released = True
        return True

    def _handle_signal(self, signum, frame):
        self.release()
        self.interrupted = True
