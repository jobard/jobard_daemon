

def walltime_to_seconds(time: str) -> int:
    return sum(x * int(t) for x, t in zip([3600, 60, 1], time.split(':')))


def seconds_to_walltime(seconds: int) -> str:
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    return f'{h:02d}:{m:02d}:{s:02d}'
