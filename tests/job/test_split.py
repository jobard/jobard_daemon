"""Test splitting utilities."""
from typing import List, Optional, Tuple

import pytest

from jobard_daemon.job.split import split
from jobard_daemon.type.basic import Split


@pytest.mark.parametrize('jobs, split_required, expected', [
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            None,
            [
                [
                    (['a'], ['aa']),
                    (['b'], ['bb']),
                    (['c'], ['cc']),
                    (['d'], ['dd']),
                    (['e'], ['ee'])
                ]
            ]
    ),
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            '6',
            [
                [
                    (['a'], ['aa']),
                    (['b'], ['bb']),
                    (['c'], ['cc']),
                    (['d'], ['dd']),
                    (['e'], ['ee'])
                ]
            ]
    ),
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            '3',
            [
                [
                    (['a'], ['aa']),
                    (['b'], ['bb']),
                    (['c'], ['cc'])
                ],
                [
                    (['d'], ['dd']),
                    (['e'], ['ee'])
                ]
            ]
    ),
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            '/3',
            [
                [
                    (['a'], ['aa']),
                    (['b'], ['bb']),
                ],
                [
                    (['c'], ['cc']),
                    (['d'], ['dd']),
                ],
                [
                    (['e'], ['ee'])
                ]
            ]
    ),
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            '/5',
            [
                [(['a'], ['aa'])],
                [(['b'], ['bb'])],
                [(['c'], ['cc'])],
                [(['d'], ['dd'])],
                [(['e'], ['ee'])],
            ]
    ),
    (
            [(['a'], ['aa']), (['b'], ['bb']), (['c'], ['cc']), (['d'], ['dd']), (['e'], ['ee'])],
            '/10',
            [
                [(['a'], ['aa'])],
                [(['b'], ['bb'])],
                [(['c'], ['cc'])],
                [(['d'], ['dd'])],
                [(['e'], ['ee'])],
            ]
    ),
])
def test_npartitions(jobs: List[Tuple[List[str], List[str]]], split_required: Optional[Split], expected: int):

    assert split(jobs, split_required) == expected
