"""App type."""
from enum import Enum


class AppType(str, Enum):
    """App types."""

    DASK = 'dask'
    DASK_JOB_ARRAY = 'dask_job_array'
    SPARK = 'spark'
    STANDALONE = 'standalone'
