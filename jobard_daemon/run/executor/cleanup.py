"""Jobard cleanup executor."""
import asyncpg

from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.utils.configuration import JobardDaemonSettings


class CleanupExecutor:
    """Cleanup executor."""

    @staticmethod
    async def async_cleanup(
            loop,
            executor_id: int,
            config: JobardDaemonSettings,
            _logger: JobardLogger,
    ):

        _logger.info('start')

        # connect to the database
        conn = await asyncpg.connect(
            dsn=config.database.dsn.format(),
            loop=loop,
            timeout=config.database.connect_timeout,
            server_settings={
                'application_name': 'jobard - parse_job_orders - daemon=' +
                                    str(config.core.daemon_id) + ' executor=' + str(executor_id)
            }
        )

        try:

            # do cancel transition if improper state
            _logger.info('correct states if needed')
            await conn.execute(
                'UPDATE "job_order" SET "state" = \'CANCELLED\''
                ' WHERE "concurrency" = 0 AND "state" = \'CANCELLATION_ASKED\'',
                timeout=config.executors.cleanup.update_missing_cancel_transition_timeout
            )
            await conn.execute(
                'UPDATE "job_array" AS "ja"'
                'SET "state" = \'CANCELLED\''
                'FROM "job_order" AS "jo"'
                'WHERE '
                '    "ja"."job_order" = "jo"."id" AND'
                '    "jo"."state" = \'CANCELLED\' AND'
                '    "ja"."state" = \'CANCELLATION_ASKED\'',
                timeout=config.executors.cleanup.update_missing_cancel_transition_timeout
            )
            await conn.execute(
                'UPDATE "job" AS "j"'
                ' SET "state" = \'CANCELLED\''
                ' FROM "job_array" AS "ja"'
                ' INNER JOIN "job_order" AS "jo" ON "ja"."job_order" = "jo"."id"'
                ' WHERE'
                '    "ja"."id" = "j"."job_array" AND'
                '    "jo"."state" = \'CANCELLED\' AND'
                '    "j"."state" = \'CANCELLATION_ASKED\'',
                timeout=config.executors.cleanup.update_missing_cancel_transition_timeout
            )

            # do purge job order history
            _logger.info('purge job order if needed')
            await conn.execute(
                "DELETE FROM \"job_order\" WHERE \"created\" < NOW() - INTERVAL '" +
                config.executors.cleanup.purge_job_order_interval + "'",
                timeout=config.executors.cleanup.purge_job_order_timeout,
            )

        finally:
            # Close the connection.
            await conn.close()

        _logger.info('end')
