"""Container class."""

from jobard_daemon.model.base import JobardModel
from jobard_daemon.type.basic import MountPoints


class DockerImageProperties(JobardModel):
    """Docker image class."""

    name: str                            # Image name
    url: str                             # URL of the image
    mount_points: MountPoints            # The mount points associated to the image
