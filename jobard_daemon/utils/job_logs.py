""" Job logs utilities """
import re
from pathlib import Path
from datetime import datetime, timezone
from typing import Optional, Tuple

# default job order name pattern
DEFAULT_JOB_ORDER_PATTERN = r'_([1|2]\d{3})((0[1-9])|(1[0-2]))([0-2][1-9]|(3[0-1]))T(([0-1]\d)|(2[0-3]))([0-5][0-9])([0-5][0-9])$'

# job order submit date format
SUBMIT_DATE_FORMAT = '%Y%m%d'

def is_default_job_order_name(
        job_order_name: str
) -> bool:
    """
    Check if the name passed in parameter is a default job order name
    Args:
        job_order_name: The name to check.
    Returns:
        true if the name passed in parameter is a default job order name.
    """
    return True if re.search(DEFAULT_JOB_ORDER_PATTERN, job_order_name) else False

def sanitize_command_alias(
        from_string: str
) -> str:
    """
    Take the basename of the command.
    """
    return str(Path(from_string).name)

def sanitize_job_prefix(
        from_string: str
) -> str:
    """
    Take the basename of the command.
    """
    return str(Path(from_string).name)

def build_job_array_log_path(
        log_chroot: Path,
        command_alias: str,
        job_order_name: Optional[str],
        job_order_id: int,
        job_array_id: int,
        try_id: int
) -> Path:
    """Build the Job array log path"""
    job_order_dir_level = f'{job_order_name}_O{job_order_id}' if job_order_name \
        else f'O{job_order_id}'

    return log_chroot \
        .joinpath(command_alias) \
        .joinpath(datetime.now(timezone.utc).strftime(SUBMIT_DATE_FORMAT)) \
        .joinpath(job_order_dir_level) \
        .joinpath(f'A{job_array_id}_T{try_id}')

def build_jobs_log_path(
        log_chroot: Path,
        command_alias: str,
        job_order_name: Optional[str],
        job_order_id: int,
        job_array_id: int,
        try_id: int
) -> Path:
    """Build the Job array daemon log path"""
    return build_job_array_log_path(log_chroot=log_chroot,
                             command_alias=command_alias,
                             job_order_name=job_order_name,
                             job_order_id=job_order_id,
                             job_array_id=job_array_id,
                             try_id=try_id).joinpath('jobs')

def build_job_log_files(
                        job_idx: int,
                        job_prefix: Optional[str]
                        ) -> Tuple[str, str]:
    """Build the job stdout and stderr filenames"""
    job_stdout_log = f'{job_prefix}_{job_idx}_stdout.log' if job_prefix \
        else f'{job_idx}_stdout.log'
    job_stderr_log = f'{job_prefix}_{job_idx}_stderr.log' if job_prefix \
        else f'{job_idx}_stderr.log'
    return job_stdout_log, job_stderr_log

def build_job_array_daemon_log_path(
        log_chroot: Path,
        command_alias: str,
        job_order_name: Optional[str],
        job_order_id: int,
        job_array_id: int,
        try_id: int
) -> Path:
    """Build the Job array daemon log path"""
    return build_job_array_log_path(log_chroot=log_chroot,
                             command_alias=command_alias,
                             job_order_name=job_order_name,
                             job_order_id=job_order_id,
                             job_array_id=job_array_id,
                             try_id=try_id).joinpath('daemon')

def build_job_array_log_file(
        daemon_log_output: Path,
        cluster_type: str,
        daemon_id: int,
        executor_id: int,
        job_order_id: int,
        job_array_id: int,
        try_id: int
) -> Path:
    """Build the job array log file"""
    return daemon_log_output.joinpath(
        'cluster_' + str(cluster_type) + '.' +
        'daemon_' + str(daemon_id) + '.' +
        'executor_' + str(executor_id) + '.' +
        'order_' + str(job_order_id) + '.' +
        'array_' + str(job_array_id) + '.' +
        'try_' + str(try_id)
    )


