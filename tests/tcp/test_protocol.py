"""Test TCP Protocol."""
import pathlib

import pytest
from pydantic import PositiveInt, confloat, NonNegativeInt

from jobard_daemon.model.event.app import AppEvent
from jobard_daemon.model.event.cancel import JobCancelEvent
from jobard_daemon.model.event.job import JobEvent
from jobard_daemon.model.event.ping import PingEvent
from jobard_daemon.model.event.progress import JobProgressEvent
from jobard_daemon.model.event.request import RequestHeaderEvent
from jobard_daemon.model.event.response import ResponseHeaderEvent
from jobard_daemon.tcp.protocol import JProtocol
from jobard_daemon.type.app import AppType
from jobard_daemon.type.basic import Arguments, Memory, Walltime
from jobard_daemon.type.cluster import Cluster
from jobard_daemon.type.op import Op
from jobard_daemon.type.state import State


@pytest.mark.parametrize('job_id, command', [
    (1, ['command', 'with', 'arguments']),
])
def test_protocol_job_event(job_id: int, command: Arguments):
    job = JobEvent(
        id=job_id,
        command=command
    )

    obj = JProtocol.decode_job_event(JProtocol.encode_job_event(job))

    assert obj.id == job.id
    assert len(obj.command) == len(job.command)


@pytest.mark.parametrize('job_id, job_progress, job_state', [
    (1, 0.1, State.SUCCEEDED),
])
def test_protocol_job_progress_event(job_id: int, job_progress: confloat(ge=0, le=1), job_state: State):
    job = JobProgressEvent(
        id=job_id,
        progress=job_progress,
        state=job_state
    )

    obj = JProtocol.decode_job_progress_event(JProtocol.encode_job_progress_event(job))

    assert obj.id == job.id
    assert obj.progress == job.progress
    assert obj.state is job.state


@pytest.mark.parametrize('job_id', [
    1,
])
def test_protocol_job_cancel_event(job_id: int):
    job = JobCancelEvent(
        id=job_id
    )

    obj = JProtocol.decode_job_cancel_event(JProtocol.encode_job_cancel_event(job))

    assert obj.id == job.id


@pytest.mark.parametrize('message', [
    'ping',
])
def test_protocol_ping_event(message: str):
    job = PingEvent(
        message=message
    )

    obj = JProtocol.decode_ping_event(JProtocol.encode_ping_event(job))

    assert obj.message == job.message


@pytest.mark.parametrize('op, obj_count', [
    (Op.PING, 1)
])
def test_protocol_request_header_event(op: str, obj_count: int):
    header = RequestHeaderEvent(
        obj_count=obj_count,
        op=op
    )

    obj = JProtocol.decode_request_header(JProtocol.encode_request_header(header))

    assert obj.op is header.op
    assert obj.obj_count == header.obj_count


@pytest.mark.parametrize('obj_count', [
    1
])
def test_protocol_response_header_event(obj_count: int):
    header = ResponseHeaderEvent(
        obj_count=obj_count
    )

    obj = JProtocol.decode_response_header(JProtocol.encode_response_header(header))

    assert obj.obj_count == header.obj_count


@pytest.mark.parametrize(
    'job_order_id, job_array_id, try_id, command_alias, name, app_type, cluster_type,'
    'driver_cores, driver_memory,'
    'worker_cores, worker_memory, n_workers, n_min_workers, n_max_workers,'
    'log_chroot,'
    'queue, walltime,'
    'application_processor_timeout, system_limit_timeout, death_timeout,'
    'driver_extra_args, worker_extra_args, jobard_extra_args, unix_uid', [
        (1, 1, 1, 'my_command', 'my_app', AppType.DASK_JOB_ARRAY, Cluster.PBS,
         1, '100MB',
         1, '2GB', 2, None, None,
         pathlib.Path('/home1/datahome/cerint/jobard/log/'),
         'sequentiel', '00:10:00',
         60, 60, 60,
         None, None, None, 1001)
    ])
def test_protocol_app_event(
        job_order_id: int,
        job_array_id: int,
        try_id: int,
        command_alias: str,
        name: str,
        app_type: AppType,
        cluster_type: Cluster,

        driver_cores: PositiveInt,
        driver_memory: Memory,

        worker_cores: PositiveInt,
        worker_memory: Memory,
        n_workers: PositiveInt,
        n_min_workers: PositiveInt,
        n_max_workers: PositiveInt,

        log_chroot: pathlib.Path,

        queue: str,
        walltime: Walltime,

        application_processor_timeout: PositiveInt,
        system_limit_timeout: PositiveInt,
        death_timeout: PositiveInt,

        driver_extra_args: Arguments,
        worker_extra_args: Arguments,
        jobard_extra_args: Arguments,

        unix_uid: NonNegativeInt,
):
    app = AppEvent(
        job_order_id=job_order_id,
        job_array_id=job_array_id,
        try_id=try_id,
        command_alias=command_alias,
        name=name,
        app_type=app_type,
        cluster_type=cluster_type,

        driver_cores=driver_cores,
        driver_memory=driver_memory,

        worker_cores=worker_cores,
        worker_memory=worker_memory,
        n_workers=n_workers,
        n_min_workers=n_min_workers,
        n_max_workers=n_max_workers,

        log_chroot=log_chroot,

        queue=queue,
        walltime=walltime,

        application_processor_timeout=application_processor_timeout,
        system_limit_timeout=system_limit_timeout,
        death_timeout=death_timeout,

        driver_extra_args=driver_extra_args,
        worker_extra_args=worker_extra_args,
        jobard_extra_args=jobard_extra_args,

        unix_uid=unix_uid,
    )

    obj = JProtocol.decode_app_event(JProtocol.encode_app_event(app))

    assert obj.job_order_id == app.job_order_id
    assert obj.job_array_id == app.job_array_id
    assert obj.try_id == app.try_id
    assert obj.command_alias == app.command_alias
    assert obj.name == app.name
    assert obj.app_type is app.app_type
    assert obj.cluster_type is app.cluster_type

    assert obj.driver_cores == app.driver_cores
    assert obj.driver_memory == app.driver_memory

    assert obj.worker_cores == app.worker_cores
    assert obj.worker_memory == app.worker_memory
    assert obj.n_workers == app.n_workers
    assert obj.n_min_workers == app.n_min_workers
    assert obj.n_max_workers == app.n_max_workers

    assert obj.log_chroot == app.log_chroot

    assert obj.queue == app.queue
    assert obj.walltime == app.walltime

    assert obj.application_processor_timeout == app.application_processor_timeout
    assert obj.system_limit_timeout == app.system_limit_timeout
    assert obj.death_timeout == app.death_timeout

    assert obj.driver_extra_args == app.driver_extra_args
    assert obj.worker_extra_args == app.worker_extra_args
    assert obj.jobard_extra_args == app.jobard_extra_args

    assert obj.unix_uid == app.unix_uid
