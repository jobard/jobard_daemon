# Jobard Daemon

Job manager of the  Job Array Daemon.

Documentation : <https://jobard.gitlab-pages.ifremer.fr/documentation/daemon/>
