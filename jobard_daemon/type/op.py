"""Tcp Op type."""
from enum import Enum


class Op(str, Enum):
    """Tcp operation between a remote executor and the Jobard Daemon."""

    # Ask for a pong response
    PING = 'ping'

    # Ask for the application configuration
    GET_APP = 'get_app'

    # Ask for the commands to execute
    GET_COMMANDS = 'get_commands'

    # Report progress for a batch of commands at once
    JOB_REPORT_PROGRESS = 'job_report_progress'

    # Cancel for a batch of job at once
    JOB_CANCEL = 'job_cancel'
