"""Reverse SSH Tunnel Config class."""
from jobard_daemon.model.base import JobardModel


class ReverseTunnelConfig(JobardModel):
    """Reverse SSH Tunnel Config class."""

    # the host and port to bind to, on the local Jobard Daemon machine
    local_host: str = '127.0.0.1'                            # host (local machine by default)
    local_port: int                                          # dynamically generated port

    # the host and port to bind to, on the remote machine
    remote_host: str = '127.0.0.1'                           # host (local machine by default)
    remote_port: int                                         # dynamically generated port
