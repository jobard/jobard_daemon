"""JobCancelEvent class."""

from jobard_daemon.model.base import JobardModel


class JobCancelEvent(JobardModel):
    """JobCancelEvent class."""

    id: int                                           # job id

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return other.id == self.id
