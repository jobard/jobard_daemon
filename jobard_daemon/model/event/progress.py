"""JobProgressEvent class that describe a progress for a particular job."""
from typing import Optional

from pydantic import confloat

from jobard_daemon.model.base import JobardModel
from jobard_daemon.type.state import State


class JobProgressEvent(JobardModel):
    """JobProgressEvent class that describe a progress for a particular job."""

    id: int                                           # job id
    progress: confloat(ge=0, le=1) = 0                # progress
    state: State = State.NEW                          # state
    job_prefix: Optional[str]                         # job log file prefix
