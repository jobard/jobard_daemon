"""Health check executor."""
import getpass
from datetime import datetime

import asyncpg

from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.type.cluster import Cluster
from jobard_daemon.type.state import State
from jobard_daemon.utils.configuration import (
    JobardDaemonSettings,
)


# global app-level postgresql lock for this type of executor
lock_process_healthcheck_test_id = 2040403043

SELECT_REMOTE_CONNECTIONS_TO_TEST = """
    SELECT 
      "merged_jobs"."conn_name" AS "merged_conn_name", 
      "merged_jobs"."daemon_id" AS "merged_daemon_id", 
      MAX("merged_jobs"."max_date_created") AS "merged_max_date_created",
      "merged_jobs"."cluster_id" AS "merged_cluster_id", 
      MAX("merged_jobs"."state") AS "merged_state",
      "merged_jobs"."healthcheck_interval" AS "merged_healthcheck_interval" 
    FROM 
      (
        -- Select connection joined with last test joborders
        SELECT 
          "jo"."connection" AS "conn_name", 
          "ja"."daemon_id" AS "daemon_id", 
          "jo"."created" AS "max_date_created", 
          "jobs_healthcheck"."cluster_id" AS "cluster_id", 
          "jo"."state" AS "state", 
          "jobs_healthcheck"."healthcheck_interval" AS  "healthcheck_interval"
        FROM 
          "job_order" AS "jo" 
          INNER JOIN "job_array" AS "ja" ON "ja"."job_order" = "jo"."id" 
          INNER JOIN (
            SELECT 
              "conn"."name" AS "conn_name", 
              "conn"."daemon_id" AS "conn_daemon_id", 
              MAX("jo"."created") AS "max_date_created", 
              "cl"."healthcheck_interval" AS "healthcheck_interval", 
              "cl"."cluster_id" AS "cluster_id" 
            FROM 
              "remote_connection" AS "conn" 
              INNER JOIN "job_order" AS "jo" ON "jo"."connection" = "conn"."name" 
              AND "conn"."daemon_id" IN (
                SELECT 
                  "ja"."daemon_id" 
                FROM 
                  "job_array" AS "ja" 
                WHERE 
                  "ja"."job_order" = "jo"."id"
              ) 
              AND "conn"."enabled" = TRUE 
              AND "jo"."is_healthcheck" = TRUE 
              INNER JOIN "cluster" AS "cl" ON "cl"."cluster_id" = "conn"."cluster_id" 
              AND "cl"."enabled" = TRUE 
            GROUP BY 
              "conn"."name", 
              "conn"."daemon_id", 
              "cl"."healthcheck_interval", 
              "cl"."cluster_id"
          ) AS "jobs_healthcheck" ON "jo"."connection" = "jobs_healthcheck"."conn_name" 
          AND "daemon_id" = "jobs_healthcheck"."conn_daemon_id" 
          AND "jo"."created" = "jobs_healthcheck"."max_date_created" 
        WHERE 
          "jo"."is_healthcheck" = TRUE 
        UNION 
        -- select connections without joborders for the case if no test joborders in database
        SELECT 
          "conn"."name" AS "conn_name", 
          "conn"."daemon_id" AS "conn_daemon_id", 
          NULL AS "max_date_created", 
          "cl"."cluster_id" AS "cluster_id", 
          NULL AS "state", 
          "cl"."healthcheck_interval" AS "healthcheck_interval" 
        FROM 
          "remote_connection" AS "conn" 
          INNER JOIN "cluster" AS "cl" ON "cl"."cluster_id" = "conn"."cluster_id" 
          AND "conn"."enabled" = TRUE 
          AND "cl"."enabled" = TRUE 
      ) AS "merged_jobs" 
    GROUP BY 
      "merged_jobs"."conn_name", 
      "merged_jobs"."daemon_id", 
      "merged_jobs"."healthcheck_interval", 
      "merged_jobs"."cluster_id"
"""

SELECT_TEST_IMAGE = """
                      SELECT 
                        "j_user"."user_id" AS "user_id",
                        CASE WHEN "cl"."type" IN ('SWARM', 'KUBE') THEN 
                                "image"."image_id"
                              ELSE
                                NULL
                              END AS "image_id"
                      FROM
                      "jobard_user" AS "j_user"
                       LEFT OUTER JOIN (
                            SELECT 
                             "docker_image"."image_id",
                             "docker_image"."user_id"
                            FROM 
                                "docker_image"
                            WHERE "docker_image"."is_test" = TRUE
                       ) AS "image" ON "j_user"."user_id" = "image"."user_id"
                      INNER JOIN "cluster_access" AS "cla" ON "j_user"."user_id" = "cla"."user_id"
                      INNER JOIN "cluster" AS "cl" ON "cl"."cluster_id" = "cla"."cluster_id"
                      WHERE "cl"."cluster_id" = $1
                      LIMIT 1
                    """

INSERT_TEST_JOBORDER = """
                        INSERT INTO "job_order"(
                            "name", "is_healthcheck", "command", "arguments", "interpolate_command", "split",
                            "priority", "cores", "memory", "walltime", "max_concurrency", "connection",
                            "job_extra", "env_extra", "user_id", "image_id"
                            )
                            VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16)
                        """


class HealthCheckExecutor:
    """Jobard health check executor."""

    @staticmethod
    async def async_asyncpg_connect(
            loop,
            config: JobardDaemonSettings,
            executor_id: int,
    ):
        """Create a connection to the database.
        Args:
            loop: The event loop.
            config: The jobard daemon config.
            executor_id: The executor id.

        Returns:
            The database connection.
        """
        # connect to the database
        return await asyncpg.connect(
            dsn=config.database.dsn.format(),
            loop=loop,
            timeout=config.database.connect_timeout,
            server_settings={
                'application_name': 'jobard - health_check - daemon=' + str(config.core.daemon_id) +
                                    ' executor=' + str(executor_id)
            }
        )

    @staticmethod
    async def async_health_check(
            loop,
            executor_id: int,
            config: JobardDaemonSettings,
            _logger: JobardLogger,
    ):
        """Process to the health check.
        Args:
            loop: The event loop.
            config: The jobard daemon config.
            executor_id: The executor id.
            _logger: The jobard logger.
        """
        _logger.info('start')

        # connect to the database
        conn = await HealthCheckExecutor.async_asyncpg_connect(loop, config, executor_id)

        try:
            # lock the transaction
            async with ((conn.transaction(isolation='read_committed', readonly=False))):
                await conn.execute(
                    'SELECT pg_advisory_xact_lock(' + str(lock_process_healthcheck_test_id) + ');', timeout=60)

                # Retrieve remote connections to check in database
                rows = await conn.fetch(SELECT_REMOTE_CONNECTIONS_TO_TEST,
                                        timeout=config.executors.healthcheck.select_remote_connections_timeout)

                # For each connection, test if there is time to do a healthcheck
                for row in rows:
                    max_date_created = row['merged_max_date_created']
                    healthcheck_interval = row['merged_healthcheck_interval']
                    connection_name = row['merged_conn_name']
                    cluster_id = row['merged_cluster_id']
                    job_state = row['merged_state']
                    # Insert a new test joborder if there is no one for this connection or
                    # if the latest finished test joborder is too old (comparing with cluster healthcheck interval)
                    if max_date_created is None \
                        or (job_state not in (State.NEW, State.ENQUEUED, State.RUNNING, State.SUBMITTED)
                            and datetime.utcnow() - max_date_created >= healthcheck_interval):
                        # Check the existence of the connection in deamon config file and retrieve test properties
                        if connection_name in config.hosts_connections and \
                                row['merged_daemon_id'] == str(config.core.daemon_id):
                            host_config = config.hosts_connections[connection_name]

                            # Retrieve the image to use for the healthcheck
                            image_row = await conn.fetchrow(
                                SELECT_TEST_IMAGE, cluster_id,
                                timeout=config.executors.healthcheck.select_test_image_timeout)

                            # check for Swarm and Kubernetes clusters if a Docker test image is defined
                            if host_config.cluster in [Cluster.SWARM, Cluster.KUBE] and image_row['image_id'] is None:
                                _logger.warn('daemon id = ' + str(config.core.daemon_id) +
                                             ' executor id = ' + str(executor_id) +
                                             f' test Docker image is mandatory for {host_config.cluster} cluster.' +
                                             ' Please create one')
                            else:
                                # Create a test joborder for this connection into the database
                                command = host_config.healthcheck.command.split(' ')
                                jo_command = [command.pop(0)]
                                jo_args = [[str(arg)] for arg in command]
                                jo_name = datetime.utcnow().strftime('{0}_%Y%m%dT%H%M%S'.format(getpass.getuser()))

                                await conn.execute(INSERT_TEST_JOBORDER,
                                                   jo_name,
                                                   True,  # test job
                                                   jo_command,
                                                   jo_args,
                                                   False,  # No interpolation
                                                   None,  # No splitting
                                                   1,  # High priority
                                                   1,  # Only one core
                                                   host_config.healthcheck.memory,
                                                   host_config.healthcheck.walltime,
                                                   1,  # Max concurrency et to 1
                                                   connection_name,
                                                   host_config.healthcheck.job_extra,
                                                   None,  # No job env options
                                                   image_row['user_id'],
                                                   image_row['image_id'],
                                                   timeout=config.executors.healthcheck.insert_job_order_timeout,
                                                   )

                                _logger.info('daemon id = ' + str(config.core.daemon_id) +
                                             ' executor id = ' + str(executor_id) +
                                             ' host config = ' + connection_name + ', ' +
                                             'creating a new job order for test')
                        # If remote connection is not found in daemon configuration file, just warn it and test the
                        # other connections
                        else:
                            _logger.warn('daemon id = ' + str(config.core.daemon_id) +
                                         ' executor id = ' + str(executor_id) +
                                         f' an error occured : remote connection {connection_name} is not found ' +
                                         'in daemon configuration')
        except Exception as error:
            _logger.warn('daemon id = ' + str(config.core.daemon_id) +
                         ' executor id = ' + str(executor_id) +
                         'an error occured = ' + str(error))
            _logger.exception(error)

        finally:
            # Close the connection.
            if conn is not None:
                await conn.close()

        _logger.info('end')
