"""JobEvent class that describe a job sent to the remote executors."""
from typing import Optional

from jobard_daemon.model.base import JobardModel
from jobard_daemon.type.basic import Arguments


class JobEvent(JobardModel):
    """JobEvent class that describe a job sent to the remote executors."""

    id: int                                           # job id
    command: Arguments                                # command to execute
    job_prefix: Optional[str]                         # job log file prefix
