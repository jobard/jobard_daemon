"""Jobard client console application."""
import logging

import click

from jobard_daemon.core.logger.log import JobardLogger
from jobard_daemon.daemon import JobardDaemon
from jobard_daemon.utils.configuration import load_daemon_sys_config


@click.group()
@click.option('--debug/--no-debug', default=False)
@click.option('--config', type=str)
@click.version_option()
@click.pass_context
def cli(ctx, debug, config):
    """

    Args:
        ctx: click ctx
        debug: enable logging.DEBUG
        config: the config filepath

    Returns:

    """
    ctx.ensure_object(dict)
    ctx.obj['debug'] = debug
    ctx.obj['config'] = config


@cli.command()
@click.pass_context
def run(ctx):
    """Report command."""

    # config
    config = load_daemon_sys_config(ctx.obj['config'])

    # logging
    logger = JobardLogger(logging.INFO if ctx.obj['debug'] is False else logging.DEBUG)
    logger.create(config.core.daemon_id)
    logger.debug(config.dict())

    # run everything
    JobardDaemon(settings=config, log=logger).run()


def main():
    """Run jobard client console script."""
    cli()


if __name__ == '__main__':
    main()
