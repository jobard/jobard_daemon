from typing import Optional

from pydantic import NonNegativeInt

from jobard_daemon.model.base import JobardModel
from pathlib import Path
from pydantic.types import SecretStr


class RemoteUserProperties(JobardModel):
    """Remote user properties class."""

    remote_username: str                        # username
    log_root_path: Path = None                  # the root log path
    remote_app_path: Path = None                # the Jobard remote dask app path
    entry_point_wrapper: Path = None            # the Jobard remote dask python interpreter path
    entry_point: Path = None                    # the Jobard remote dask entry point path
    remote_password: SecretStr = None           # password
    remote_client_key: Optional[str]            # alternatively, use private key authentication
    unix_uid: NonNegativeInt = None             # the unix uid for clusters SWARM/KUBE
